/*
 * objednavkaitem.h
 *
 *  Created on: 9.5.2014
 *      Author: benecore
 */

#ifndef OBJEDNAVKAITEM_H_
#define OBJEDNAVKAITEM_H_

#include <QObject>
#include <QString>
#include <QDebug>

class ObjednavkaItem : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString dietaID READ dietaID WRITE setDietaID NOTIFY dataChanged)
	Q_PROPERTY(QString preID READ preID WRITE setPreID NOTIFY dataChanged)
	Q_PROPERTY(QString datum READ datum WRITE setDatum NOTIFY dataChanged)
	Q_PROPERTY(QString dlzkaProgramuID READ dlzkaProgramuID WRITE setDlzkaProgramuID NOTIFY dataChanged)
	Q_PROPERTY(QString dorucenieID READ dorucenieID WRITE setDorucenieID NOTIFY dataChanged)
	Q_PROPERTY(QString mestoID READ mestoID WRITE setMestoID NOTIFY dataChanged)
	Q_PROPERTY(QString pobockaID READ pobockaID WRITE setPobockaID NOTIFY dataChanged)
	Q_PROPERTY(QString casDoruceniaID READ casDoruceniaID WRITE setCasDoruceniaID NOTIFY dataChanged)
	Q_PROPERTY(QString meno READ meno WRITE setMeno NOTIFY dataChanged)
	Q_PROPERTY(QString priezvisko READ priezvisko WRITE setPriezvisko NOTIFY dataChanged)
	Q_PROPERTY(QString telefon READ telefon WRITE setTelefon NOTIFY dataChanged)
	Q_PROPERTY(QString email READ email WRITE setEmail NOTIFY dataChanged)
	// FAKTURACNA ADRESA
	Q_PROPERTY(QString faMeno READ faMeno WRITE setFaMeno NOTIFY dataChanged)
	Q_PROPERTY(QString faUlica READ faUlica WRITE setFaUlica NOTIFY dataChanged)
	Q_PROPERTY(QString faObec READ faObec WRITE setFaObec NOTIFY dataChanged)
	Q_PROPERTY(QString faPsc READ faPsc WRITE setFaPsc NOTIFY dataChanged)
	Q_PROPERTY(QString faIc READ faIc WRITE setFaIc NOTIFY dataChanged)
	Q_PROPERTY(QString faDic READ faDic WRITE setFaDic NOTIFY dataChanged)
	// --- //
	Q_PROPERTY(QString rovnakaAdresa READ rovnakaAdresa WRITE setRovnakaAdresa NOTIFY dataChanged)
	// DODACIA ADRESA
	Q_PROPERTY(QString doMeno READ doMeno WRITE setDoMeno NOTIFY dataChanged)
	Q_PROPERTY(QString doUlica READ doUlica WRITE setDoUlica NOTIFY dataChanged)
	Q_PROPERTY(QString doObec READ doObec WRITE setDoObec NOTIFY dataChanged)
	Q_PROPERTY(QString doPsc READ doPsc WRITE setDoPsc NOTIFY dataChanged)
	// --- //
	Q_PROPERTY(QString mamVoucher READ mamVoucher WRITE setMamVoucher NOTIFY dataChanged)
	Q_PROPERTY(QString voucherKod READ voucherKod WRITE setVoucherKod NOTIFY dataChanged)
	Q_PROPERTY(QString poznamka READ poznamka WRITE setPoznamka NOTIFY dataChanged)

public:
	ObjednavkaItem(QObject *parent = 0);
	virtual ~ObjednavkaItem();


	signals:
	void dataChanged();


public slots:
	inline QString dietaID() const { return _dietaID; }
	inline void setDietaID(const QString &value){
		if (_dietaID != value){
			_dietaID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString preID() const { return _preKohoID; }
	inline void setPreID(const QString &value){
		if (_preKohoID != value){
			_preKohoID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString datum() const { return _datum; }
	inline void setDatum(const QString &value){
		if (_datum != value){
			_datum = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString dlzkaProgramuID() const { return _dlzkaProgramuID; }
	inline void setDlzkaProgramuID(const QString &value){
		if (_dlzkaProgramuID != value){
			_dlzkaProgramuID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString dorucenieID() const { return _dorucenieID; }
	inline void setDorucenieID(const QString &value){
		if (_dorucenieID != value){
			_dorucenieID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString mestoID() const { return _mestoID; }
	inline void setMestoID(const QString &value){
		if (_mestoID != value){
			_mestoID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString pobockaID() const { return _pobockaID; }
	inline void setPobockaID(const QString &value){
		if (_pobockaID != value){
			_pobockaID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString casDoruceniaID() const { return _casDoruceniaID; }
	inline void setCasDoruceniaID(const QString &value){
		if (_casDoruceniaID != value){
			_casDoruceniaID = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString meno() const { return _meno; }
	inline void setMeno(const QString &value){
		if (_meno != value){
			_meno = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString priezvisko() const { return _priezvisko; }
	inline void setPriezvisko(const QString &value){
		if (_priezvisko != value){
			_priezvisko = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString telefon() const { return _telefon; }
	inline void setTelefon(const QString &value){
		if (_telefon != value){
			_telefon = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString email() const { return _email; }
	inline void setEmail(const QString &value){
		if (_email != value){
			_email = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	// Fakturacna adresa
	inline QString faMeno() const { return _faMeno; }
	inline void setFaMeno(const QString &value){
		if (_faMeno != value){
			_faMeno = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString faUlica() const { return _faUlica; }
	inline void setFaUlica(const QString &value){
		if (_faUlica != value){
			_faUlica = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString faObec() const { return _faObec; }
	inline void setFaObec(const QString &value){
		if (_faObec != value){
			_faObec = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString faPsc() const { return _faPsc; }
	inline void setFaPsc(const QString &value){
		if (_faPsc != value){
			_faPsc = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString faIc() const { return _faIC; }
	inline void setFaIc(const QString &value){
		if (_faIC != value){
			_faIC = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString faDic() const { return _faDic; }
	inline void setFaDic(const QString &value){
		if (_faDic != value){
			_faDic = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	// --- //
	inline QString rovnakaAdresa() const { return _rovnakaAdresa; }
	inline void setRovnakaAdresa(const QString &value){
		if (_rovnakaAdresa != value){
			_rovnakaAdresa = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	// DODACIA ADRESA
	inline QString doMeno() const { return _doMeno; }
	inline void setDoMeno(const QString &value){
		if (_doMeno != value){
			_doMeno = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString doUlica() const { return _doUlica; }
	inline void setDoUlica(const QString &value){
		if (_doUlica != value){
			_doUlica = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString doObec() const { return _doObec; }
	inline void setDoObec(const QString &value){
		if (_doObec != value){
			_doObec = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString doPsc() const { return _doPsc; }
	inline void setDoPsc(const QString &value){
		if (_doPsc != value){
			_doPsc = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	// --- //
	inline QString mamVoucher() const { return _mamVoucher; }
	inline void setMamVoucher(const QString &value){
		if (_mamVoucher != value){
			_mamVoucher = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString voucherKod() const { return _voucherKod; }
	inline void setVoucherKod(const QString &value){
		if (_voucherKod != value){
			_voucherKod = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}
	inline QString poznamka() const { return _poznamka; }
	inline void setPoznamka(const QString &value){
		if (_poznamka != value){
			_poznamka = value;
			qDebug() << Q_FUNC_INFO << "-" << value;
			emit dataChanged();
		}
	}

	private:
	Q_DISABLE_COPY(ObjednavkaItem)
	QString _dietaID;
	QString _preKohoID;
	QString _datum; // 1.1.2000
	QString _dlzkaProgramuID;
	QString _dorucenieID;
	QString _mestoID;
	QString _pobockaID;
	QString _casDoruceniaID;
	QString _meno;
	QString _priezvisko;
	QString _telefon;
	QString _email;
	// Fakturacna adresa
	QString _faMeno;
	QString _faUlica;
	QString _faObec;
	QString _faPsc;
	QString _faIC;
	QString _faDic;
	// --- //
	QString _rovnakaAdresa;
	// Dodacia adresa
	QString _doMeno;
	QString _doUlica;
	QString _doObec;
	QString _doPsc;
	// --- //
	QString _mamVoucher;
	QString _voucherKod;
	QString _poznamka;

};

#endif /* OBJEDNAVKAITEM_H_ */
