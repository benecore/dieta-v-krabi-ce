import bb.cascades 1.2

import "../components"
import "../items"


Sheet {
    id: root
    property variant indexPath: null
    property ArrayDataModel model: null;
    
    onClosed: {
        destroy()
    }
    
    
    onOpened: {
        listView.scrollToItem(root.indexPath,ScrollAnimation.Smooth)
    }
    
    
    BasePage {
        id: page
        
        
        /*header: TitleHeader {
            headerText: activeTab.title
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }*/
        backgroundColor: Color.create("#f8f8f8")
        
        
        ListView {
            id: listView
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            dataModel: model
            layout: StackListLayout {
                headerMode: ListHeaderMode.None
                orientation: LayoutOrientation.LeftToRight

            }
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    ListItemFotogaleria {
                        scale: ScalingMethod.AspectFill
                        preferredWidth: Qt.Size.maxWidth
                        preferredHeight: Qt.Size.maxHeight
                    }
                }
            ]
            flickMode: FlickMode.SingleItem
        } // end of listView
        
        Button {
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Top
            text: "Zavřít"
            onClicked: {
                root.close()
            }
            overlapTouchPolicy: OverlapTouchPolicy.Allow
        } // end of close button
        
        
    } // end of page
    
}