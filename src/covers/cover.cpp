/*
 * cover.cpp
 *
 *  Created on: 8.5.2014
 *      Author: benecore
 */

#include "cover.h"

Cover::Cover(QObject *parent) :
SceneCover(parent)
{
	qml = QmlDocument::create("asset:///Cover.qml").parent(this);
	_container = qml->createRootObject<Container>();

	// Set the content of Frame
	setContent(_container);
}

Cover::~Cover()
{
	delete _container;
	delete qml;
}

