/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2
import "tabs"
import "pages"
import "actions"

TabbedPane {
    
    
    Menu.definition: MenuDefinition{
        actions: [
            AboutAction {
                id: aboutAction
                onClicked: {
                    activePane.push(aboutPage.createObject())
                }
                attachedObjects: [
                    ComponentDefinition {
                        id: aboutPage
                        AboutPage {
                            
                        }
                    }
                ]
            }
        ]
    }
    
    
    showTabsOnActionBar: false
    
    
    Tab {
        id: homeTab
        title: "Úvod"
        imageSource: "asset:///images/tabs/home-icon.png"
        delegate: Delegate {
            Home {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateImmediately
    }
    Tab {
        id: myAccount
        title: "Můj účet"
        imageSource: "asset:///images/tabs/ucet.png"
        delegate: Delegate {
            MyAccount {
            
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: naseProgramyTab
        title: "Naše programy"
        imageSource: "asset:///images/tabs/nase-programy.png"
        delegate: Delegate {
            NaseProgramy {
            
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: jedalnicekTab
        title: "Jidelníček"
        imageSource: "asset:///images/tabs/jedalnicek.png"
        delegate: Delegate {
            Jedalnicek {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: fotogaleriaTab
        title: "Fotogalerie"
        imageSource: "asset:///images/tabs/fotogaleria.png"
        delegate: Delegate {
            Fotogaleria {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: cenikTab
        title: "Ceník"
        imageSource: "asset:///images/tabs/cenik.png"
        delegate: Delegate {
            Cenik {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: objednavkaTab
        title: "Objednávka"
        imageSource: "asset:///images/tabs/objednavka.png"
        delegate: Delegate {
            Objednavka {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.Default
    }
    Tab {
        id: osobnyOdberTab
        title: "Osobní odběr"
        imageSource: "asset:///images/tabs/osobny-odber.png"
        delegate: Delegate {
            OsobnyOdber {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: dopravaTab
        title: "Doprava"
        imageSource: "asset:///images/tabs/doprava.png"
        delegate: Delegate {
            Doprava {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: referencieTab
        title: "Reference"
        imageSource: "asset:///images/tabs/referencie.png"
        delegate: Delegate {
            Referencie {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    Tab {
        id: pripravaJedalTab
        title: "Příprava jídel"
        imageSource: "asset:///images/tabs/priprava-jedal.png"
        delegate: Delegate {
            PripravaJedal {
                
            }
        }
        delegateActivationPolicy: TabDelegateActivationPolicy.ActivateWhenSelected
    }
    
    onCreationCompleted: {
        Qt.App = App
        Qt.Api = Api
        Qt.Size = Size
        Qt.DB = DB
        Qt.Settings = Settings
        Qt.Desc = Desc
        Qt.Helper = Helper
        Qt.Cacher = Cacher
    }
}
