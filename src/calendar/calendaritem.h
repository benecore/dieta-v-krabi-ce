/*
 * calendaritem.h
 *
 *  Created on: 24.4.2014
 *      Author: benecore
 */

#ifndef CALENDARITEM_H_
#define CALENDARITEM_H_

#include <QObject>
#include <QVariantMap>

class CalendarItem : public QObject {
	Q_OBJECT
	Q_PROPERTY(int index READ index CONSTANT)
	Q_PROPERTY(bool today READ today CONSTANT)
	Q_PROPERTY(QVariant day READ day CONSTANT)
	Q_PROPERTY(QVariant dayName READ dayName CONSTANT)
	Q_PROPERTY(QVariant dayLongName READ dayLongName CONSTANT)
	Q_PROPERTY(QVariant date READ date CONSTANT)
	//Q_PROPERTY(QVariantMap data READ data WRITE setData NOTIFY dataChanged)
	Q_PROPERTY(QVariantMap data READ data CONSTANT)
	//Q_PROPERTY(bool hasData READ hasData CONSTANT)
	Q_PROPERTY(bool hasData READ hasData NOTIFY dataChanged)
	Q_PROPERTY(bool canEdit READ canEdit CONSTANT)
public:
	CalendarItem(const QVariantMap &data, QObject *parent = 0);
	virtual ~CalendarItem();



	inline int index() const { return _data.value("index").toInt(); }
	inline bool today() const { return _data.value("today").toBool(); }
	inline QVariant day() const { return _data.value("day"); }
	inline QVariant dayName() const { return _data.value("dayName"); }
	inline QVariant dayLongName() const { return _data.value("dayLongName"); }
	inline QVariant date() const { return _data.value("date"); }
	inline QVariantMap data() const { return _data.value("data").toMap(); }
	inline void setData(const QVariantMap &value){
		if (_data.value("data").toMap() != value){
			_data.insert("data", value);
			emit dataChanged();
		}
	}
	inline bool hasData() const { return _data.value("data").toMap().isEmpty() ? false : true; }
	inline bool canEdit() const { return _data.value("canEdit").toBool(); }



	signals:
	void dataChanged();


private:
	Q_DISABLE_COPY(CalendarItem)
QVariantMap _data;

};

#endif /* CALENDARITEM_H_ */
