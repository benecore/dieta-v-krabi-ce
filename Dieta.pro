APP_NAME = Dieta

QT += network sql
CONFIG += qt warn_on cascades10 debug_and_release
LIBS += -lbb -lbbsystem -lbbdevice -lbbplatform -lbbdata -lbbcascadesmaps -lQtLocationSubset

INCLUDEPATH += ../src   SOURCES += ../src/*.cpp
HEADERS += ../src/*.h
 
device {
    CONFIG(release, debug|release) {
        DESTDIR = o.le-v7
        TEMPLATE=lib
        QMAKE_CXXFLAGS += -fvisibility=hidden
    }
    CONFIG(debug, debug|release) {
        DESTDIR = o.le-v7-g
    }
}
 
simulator {
    CONFIG(release, debug|release) {
        DESTDIR = o
    }
    CONFIG(debug, debug|release) { 
        DESTDIR = o-g
    } 
}
 
OBJECTS_DIR = $${DESTDIR}/.obj
MOC_DIR = $${DESTDIR}/.moc
RCC_DIR = $${DESTDIR}/.rcc
UI_DIR = $${DESTDIR}/.ui


include(config.pri)