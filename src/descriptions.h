/*
 * descriptions.h
 *
 *  Created on: 7.4.2014
 *      Author: benecore
 */

#ifndef DESCRIPTIONS_H_
#define DESCRIPTIONS_H_

#include <QObject>

class Descriptions : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QString hubnuZenaText READ hubnuZenaText CONSTANT)
	Q_PROPERTY(QString hubnuMuzText READ hubnuMuzText CONSTANT)
	Q_PROPERTY(QString zzdraveZenaText READ zzdraveZenaText CONSTANT)
	Q_PROPERTY(QString zzdraveMuzText READ zzdraveMuzText CONSTANT)
	Q_PROPERTY(QString pripravaJedalText READ pripravaJedalText CONSTANT)
	Q_PROPERTY(QString dopravaText READ dopravaText CONSTANT)
	Q_PROPERTY(QString jandovaText READ jandovaText CONSTANT)
	Q_PROPERTY(QString solarikovaText READ solarikovaText CONSTANT)
	Q_PROPERTY(QString stoudkovaText READ stoudkovaText CONSTANT)
public:
	Descriptions(QObject *parent = 0) : QObject(parent) {}

	inline const QString hubnuZenaText() const {
		return trUtf8("<html>Časy, kdy hubnout znamenalo obmotat ledničku řetězem a jíst pouze nevýrazná jídla bez chuti, jsou dávno pryč. "
				"Díky programu DIETAVKRABIČCE.CZ nemusíte zanevřít na své gurmetství a přitom posunete rafičku na váze o několik stupínku doleva."
				"<br/><br/>Naše klientky dosahují po <b>měsíční kúře</b> průměrného váhového úbytku <b>5–7 kg</b>. "
				"U každé je samozřejmě úbytek individuální.<br/><br/>Každý den <b>jídelníček zahrnuje:</b><br/>"
				"snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 5 000 kJ.</html>");
	}
	inline const QString hubnuMuzText() const {
		return trUtf8("<html>Časy, kdy hubnout znamenalo obmotat ledničku řetězem a jíst pouze nevýrazná jídla bez chuti, jsou dávno pryč. "
				"Díky programu DIETAVKRABIČCE.CZ nemusíte zanevřít na své gurmetství a přitom posunete rafičku na váze o několik stupínku doleva."
				"<br/><br/>Naši klienti dosahují po <b>měsíční kúře</b> průměrného váhového úbytku <b>5–7 kg</b>. "
				"U každé je samozřejmě úbytek individuální.<br/><br/>Každý den <b>jídelníček zahrnuje:</b><br/>"
				"snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 7 000 kJ.</html>");
	}
	inline const QString zzdraveZenaText() const {
		return trUtf8("<html>Pokud patříte k té šťastnější části populace, která se při pohledu na váhu spokojeně usměje, je pro vás program ŽIJU ZDRAVĚ jako stvořený."
				"Tento jídelníček se totiž nesnaží příjem potravy omezit, mnohem více dbá o pestrost a kvalitu stravy."
				"<br/><br/>Program ŽIJU ZDRAVĚ plynule navazuje na program HUBNU. Již se nejedná o redukční jídelníček, ale o udržovací.<br/><br/>"
				"Každý den <b>jídelníček zahrnuje:</b><br/>snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 8 000 kJ.</html>");
	}
	inline const QString zzdraveMuzText() const {
		return trUtf8("<html>Pokud patříte k té šťastnější části populace, která se při pohledu na váhu spokojeně usměje, je pro vás program ŽIJU ZDRAVĚ jako stvořený. "
				"Tento jídelníček se totiž nesnaží příjem potravy omezit, mnohem více dbá o pestrost a kvalitu stravy."
				"<br/><br/>Program ŽIJU ZDRAVĚ plynule navazuje na program HUBNU. Již se nejedná o redukční jídelníček, ale o udržovací.<br/><br/>"
				"Každý den <b>jídelníček zahrnuje:</b><br/>snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 10 000 kJ.</html>");
	}
	inline const QString pripravaJedalText() const {
		return trUtf8("<html>Naše krabičková dieta je připravována výhradně <b>z čerstvých a kvalitních surovin pod přísným hygienickým dohledem.</b><br/><br/>"
				"Všechna jídla jsou vyvíjená <b>profesionálním týmem kuchařů</b> s bohatými zkušenostmi s domácí i mezinárodní kuchyní.<br/><br/>"
				"Pro přípravu krabičkové diety využíváme <b>nejmodernějších přístrojů</b>. Veškeré vybavení naší kuchyně splňuje ty nejnáročnější požadavky moderního a zdravého zařízení.<br/><br/>"
				"Naše výrobna je <b>držitelem certifikátu HACCP</b>. Tento certifikát zaručuje bezpečnost a především vysokou kvalitu našich programů ve všech fázích manipulace s pokrmy, surovinami, skladováním a distribucí.<br/><br/>"
				"Jako jediná společnost na trhu krabičkových diet se v současné době certifikujeme na <b>ISO normu 22000</b>, což je nejvyšší možná mezinárodní norma, která specifikuje požadavky na systém bezpečnosti potravin pro všechny "
				"organizace v potravinovém řetězci, které musí být naplněny, aby byla zajištěna bezpečnost potravin.<br/><br/>"
				"<b>Adresa výrobny:</b><br/>K letišti 1018/55<br/>Praha 6, 16008</html>");
	}
	inline const QString dopravaText() const {
		return trUtf8("<html>Dietu v krabičce Vám doručíme na Vámi určené místo v regionech Praha, Praha-východ a Praha-západ nebo si jí můžete <b>ZDARMA</b> osobně vyzvednout na na našich odběrných místech. Seznam našich odběrných míst naleznete v sekci <b>OSOBNÍ ODBĚR</b>.</html>");
	}
	inline const QString jandovaText() const {
		return trUtf8("Zpěvačka Marta Jandová vyzkoušela krabičkovou dietu i se svým přítelem. ,, Jelikož mám velice nabitý pracovní program a jsem stále na cestách, krabičková dieta DIETAVKRABICCE.CZ je pro mě ideální. Je to velice pohodlné a zároveň zdravé stravování. Jelikož nemám moc času na vaření, krabičkovou dietu jsem objednala i pro svého přítele. Jídlo je moc chutné, dokonce i kolegové z muzikálu mi ho záviděli.");
	}
	inline const QString solarikovaText() const {
		return trUtf8("Patricie se pro DIETAVKRABICCE.CZ rozhodla po loňském pobytu ve Spojených státech Amerických, kde nabrala pár kil, se kterými se rozhodla zatočit. ,,Popustila jsem uzdu a jednoho dne jsem si řekla : Už dost!“ ,,Jelikož mi hodně času zabírá studium vysoké školy a natáčení seriálu Ulice a nemám tedy dostatek času na vaření, rozhodla jsem se do toho jít s DIETAVKRABICCE.CZ.“ ,,Za měsíc jsem shodila 3,5kg a tím jsem se dostala na ideální váhu, větší úbytek váhy po jednom měsíci by pro mě nebyl ani zdravý a hrozilo by mi riziko JOJO efektu, takto můj úspěch zhodnotil výživový poradce Mgr. Tomáš Lavický.“ ,,Krabičková dieta opravdu nepatří mezi ty drastické diety, měla jsem pocit, že celý den jím, sice po menších porcích, ale za to často,“ směje se Patricie.");
	}
	inline const QString stoudkovaText() const {
		return trUtf8("Míša se DIETAVKRABICCE.CZ rozhodla vyzkoušet v období, kdy se musela učit na státnice na vysoké škole. ,,Měla jsem čas pouze na učení, proto jsem nebyla schopná si sama vařit. Každý moc dobře ví, že pro práci modelky je štíhlá postava velice důležitá, vsadila jsem tedy na krabičkovou dietu a musím říct, že jsem byla moc spokojená, je to velice pohodlný a zdravý způsob stravování, nejraději bych tohoto ,,full servisu“ využívala pořád“, dodává Míša.");
	}



private:
	Q_DISABLE_COPY(Descriptions)
};


#endif /* DESCRIPTIONS_H_ */
