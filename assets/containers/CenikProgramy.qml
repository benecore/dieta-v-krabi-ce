import bb.cascades 1.2

import "../components"
import "../actions"
import "../items"
import "../pages"

Container {
    id: root
    
    
    onCreationCompleted: {
        var values = [{"title": "Hubnu žena", "icon": "asset:///images/programy/hz-icon.png"},
            {"title": "Hubnu muž", "icon": "asset:///images/programy/hm-icon.png"},
            {"title": "Žiju zdravě žena", "icon": "asset:///images/programy/zzz-icon.png"},
            {"title": "Žiju zdravě muž", "icon": "asset:///images/programy/zzm-icon.png"}]
        model.append(values)
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: hubnuZena
            CenikHubnuZena {
                
            }
        },
        ComponentDefinition {
            id: hubnuMuz
            CenikHubnuMuz {
            
            }
        },
        ComponentDefinition {
            id: zijuZdraveZena
            CenikZijuZdraveZena {
            
            }
        },
        ComponentDefinition {
            id: zijuZdraveMuz
            CenikZijuZdraveMuz {
            
            }
        }
    ]
    
    ListView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model
        }
        listItemComponents: [
            ListItemComponent {
                type: ""
                ListItemCenikProgramy {}
            }
        ]
        onTriggered: {
            clearSelection()
            select(indexPath)
            var page;
            var item = dataModel.data(indexPath)
            if (item){
                switch (parseInt(indexPath)){
                    case 0:
                        page = hubnuZena.createObject()
                        break;
                    case 1:
                        page = hubnuMuz.createObject()
                        break;
                    case 2:
                        page = zijuZdraveZena.createObject()
                        break;
                    case 3:
                        page = zijuZdraveMuz.createObject()
                        break;
                    default:
                        break;
                }
                page.title = item.title.toUpperCase()
                page.popDone.connect(clearSelection)
                activePane.push(page);
            }
        }
    } // end of listview

}
