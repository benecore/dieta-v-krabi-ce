import bb.cascades 1.2


Page {
    id: root
    
    signal popDone()
 
    property bool active: false
    property alias defaultLayout: rootContainer.layout
    property alias header: root.titleBar
    property alias backgroundImage: backgroundSource.imageSource
    property alias backgroundColor: rootContainer.background
    default property alias contentItems: rootContainer.controls
    
    actionBarAutoHideBehavior: Size.nType ? ActionBarAutoHideBehavior.HideOnScroll : ActionBarAutoHideBehavior.Default
    actionBarVisibility: ChromeVisibility.Default
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: "Zpět"
            onTriggered: {
                console.log("BACK CLICKED")
                if (Api.loading)
                	Api.loading = false
                activePane.pop()
            }
        }
    }
    
    
    Container {
        id: rootContainer
        layout: DockLayout{}
        background: SystemDefaults.Paints.ContainerBackground
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        ImageView {
            id: backgroundSource
            preferredWidth: Size.maxWidth
            preferredHeight: Size.maxHeight
            scalingMethod: ScalingMethod.AspectFill
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
        }
    }
}
