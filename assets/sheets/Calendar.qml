import bb.cascades 1.2
import com.devpda.models 1.0

import "../components"
import "../items"

Sheet {
    
    onClosed: {
        destroy()
    }
    
    Page {
        titleBar: TitleHeader {
            headerText: "Test".toUpperCase()
        }
        
        
        Container {
            
            
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: "Close"
                onClicked: {
                    close()
                }
            }
            
            ListView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                onCreationCompleted: {
                    model.generateModel()
                }
                dataModel: CalendarModel {
                    id: model
                }
                layout: GridListLayout {
                    columnCount: 7

                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        ListItemCalendarDay{}
                    }
                ]
            } // end of listView
        }

    }
}
