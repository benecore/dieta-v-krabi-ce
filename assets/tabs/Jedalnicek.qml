import bb.cascades 1.2

import "../components"
import "../actions"
import "../items"
import "../pages"

TabPage {
    id: root
    
    
    BasePage {
        id: page
        
        
        function jedalnicekDone(response){
            model.clear()
            var json = JSON.parse(response)
            tyzden = json.tyden
            count = json.count
            for (var item in json.data){
                var den = json.data[item]
                den.expanded = false
                model.append(den)
            }
        }
        
        
        property string tyzden
        property int count
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        
        onCreationCompleted: {
            App.jedalnicekDone.connect(jedalnicekDone)
            
        }
        
        onActiveChanged: {
            if (active){
                vyberTyzdna.selectedIndex = 1
            }
        }
        
        attachedObjects: [
            ComponentDefinition {
                id: dialog
                JedalnicekDialog {
                    onClosed: {
                        listView.clearSelection()
                        close()
                    }
                }
            }
        ]
        
        
        ActivityIndicator {
            id: indicator
            preferredHeight: 200
            preferredWidth: 200
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            visible: Api.loading
            running: visible
        }
        
        
        Container {
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            SegmentedControl {
                id: vyberTyzdna
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: "Minulý týden"
                        value: 2
                    },
                    Option {
                        text: "Tento týden"
                        value: 0
                    },
                    Option {
                        text: "Budoucí týden"
                        value: 1
                    }
                ]
                onSelectedOptionChanged: {
                    Api.jedalnicek(selectedValue)
                }
            } // end of SegmentControl
            
            MyLabel {
                opacity: Api.loading ? 0 : 1
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoMedium"
                fontSize: FontSize.Large
                text: page.tyzden
            }
            
            ListView {
                id: listView
                opacity: Api.loading ? 0 : 1
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
                dataModel: ArrayDataModel {
                    id: model
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ListItemJedalnicek {}
                    }
                ]
                scrollIndicatorMode: ScrollIndicatorMode.None
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    var item = dataModel.data(indexPath)
                    if (item){
                        var di = dialog.createObject()
                        di.data = item
                        di.open()
                    }
                }
            } // end of listView
            
        } // end of root container
        
        
    } // end of page
    
}