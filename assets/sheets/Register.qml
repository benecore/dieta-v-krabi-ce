import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../items"

Sheet {
    id: root
    
    
    signal done(string text)
    
    onClosed: {
        destroy()
    }
    
    
    BasePage {
        id: page
        
        function register(){
            if (email.text.length && meno.text.length && priezvisko.text.length && telefon.text.length){
                Api.registrovat(email.text.trim(), meno.text.trim(), priezvisko.text.trim(), telefon.text.trim())
            }else{
                if (!email.text.length){
                    toast.body = "Zadejte svůj email"
                    email.requestFocus()
                }else if (!meno.text.length){
                    toast.body = "Zadejte své jméno"
                    meno.requestFocus()
                }else if (!priezvisko.text.length){
                    toast.body = "Zadejte své příjmení"
                    priezvisko.requestFocus()
                }else if (!telefon.text.length){
                    toast.body = "Zadejte svůj telefon"
                    telefon.requestFocus()
                }
                toast.show()
            }
        }
        
        
        function registraciaDone(response){
            var json = JSON.parse(response)
            if (json.error == "OK"){
                DB.setLogin(json.email.trim(), json.password.trim())
                Api.setLoginDetails(json.email.trim(), json.password.trim())
                root.done("Registrace byla úspěšná")
                root.close()
            }else{
                toast.body = json.error
                toast.show()
            }
        }
        
        
        header: TitleHeader {
            headerText: "REGISTRACE"
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        
        onCreationCompleted: {
            App.registraciaDone.connect(registraciaDone)
        }
        
        
        attachedObjects: [
            SystemToast {
                id: toast
                autoUpdateEnabled: true
                button.label: "Zavřít"
                onFinished: {
                    if (value == SystemUiResult.ButtonSelection){
                        cancel()
                    }
                }
            }
        ]
        
        
        ActivityIndicator {
            preferredHeight: 200
            preferredWidth: 200
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            running: Api.loading
            visible: running
        }
        
        
        ScrollView {
            enabled: !Api.loading
        	horizontalAlignment: HorizontalAlignment.Fill
        	verticalAlignment: VerticalAlignment.Fill
            scrollViewProperties.pinchToZoomEnabled: false
            
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                
                
                TextField {
                    id: email
                    hintText: "E-mail"
                    inputMode: TextFieldInputMode.EmailAddress
                    input.submitKey: SubmitKey.Next
                    input.onSubmitted: {
                        meno.requestFocus()
                    }
                }
                
                TextField {
                    id: meno
                    hintText: "Jméno"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    input.onSubmitted: {
                        priezvisko.requestFocus()
                    }
                }
                
                TextField {
                    id: priezvisko
                    hintText: "Příjmení"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    input.onSubmitted: {
                        telefon.requestFocus()
                    }
                }
                
                TextField {
                    id: telefon
                    hintText: "Telefon"
                    inputMode: TextFieldInputMode.PhoneNumber
                    input.submitKey: SubmitKey.Next
                    input.onSubmitted: {
                        email.requestFocus()
                    }
                }
                
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    
                    Button {
                        text: "Zavřít"
                        onClicked: {
                            close()
                        }
                    }
                    
                    Button {
                        text: "Registrovat"
                        onClicked: {
                            page.register()
                        }
                    }
                } // end of buttons
                
                
            } // end of root container
            
        } // end of scroll view
    
    
    } // end of Base page

}