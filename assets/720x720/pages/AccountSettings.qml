import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../actions"
import "../items"

BasePage {
    id: root
    
    function ucetDone(response){
        var json = JSON.parse(response)
        meno = json.jmeno
        priezvisko = json.prijmeni
        telefon = json.mobil
        adresy = json.data
        model.clear()
        model.append({"title": "Nastavení adres", "subtitle": "Nastavení doručovacích adres"})
    }
    
    function zmenitNastavenie(response){
        var json = JSON.parse(response)
        toast.body = json.data
        toast.show()
    }
    
    property string meno
    property string priezvisko
    property string telefon
    property variant adresy
    header: TitleHeader {
        headerText: "Nastavení účtu".toUpperCase()
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    } 
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        Api.loading = true
        App.mojUcetDone.connect(ucetDone)
        App.zmenitNastavenieDone.connect(zmenitNastavenie)
    }
    
    onActiveChanged: {
        if (active){
            Api.mojUcet()
        }
    }
    
    
    actions: [
        RefreshAction {
            onClicked: {
                Api.mojUcet()
            }
        }
    ]
    
    attachedObjects: [
        SystemToast {
            id: toast
            button.label: ""
            autoUpdateEnabled: true
        },
        ComponentDefinition {
            id: adresyNastavenie
            AccountAdress {
                onPopDone: {
                	listView.clearSelection()
                }
            }
        }
    ]
    
    
    ActivityIndicator {
        preferredHeight: 200
        preferredWidth: 200
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        visible: Api.loading
        running: visible
    }
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        Container {
            opacity: Api.loading ? 0 : 1
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            
            
            Container {
                id: menoContainer
                MyLabel {
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Medium
                    text: "Jméno"
                    bottomMargin: 0
                }
                TextField {
                    id: menoField
                    topMargin: 0
                    hintText: "Jméno"
                    text: meno
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Done
                }
            } // end of meno container
            Container {
                id: priezviskoContainer
                MyLabel {
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Medium
                    text: "Příjmení"
                    bottomMargin: 0
                }
                TextField {
                    id: prizviskoField
                    topMargin: 0
                    hintText: "Příjmení"
                    text: priezvisko
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Done
                }
            } // end of priezvisko container
            Container {
                id: telefonContainer
                MyLabel {
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Medium
                    text: "Telefon"
                    bottomMargin: 0
                }
                TextField {
                    id: telefonField
                    topMargin: 0
                    hintText: "Telefon"
                    text: telefon
                    inputMode: TextFieldInputMode.PhoneNumber
                    input.submitKey: SubmitKey.Done
                }
            } // end of meno container
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                text: "Uložit změny"
                onClicked: {
                    if (meno != menoField.text.trim() || 
                    priezvisko != prizviskoField.text.trim() ||
                    telefon != telefonField.text.trim()){
                        Api.zmenitNastavenie(menoField.text.trim(),
                        prizviskoField.text.trim(), telefonField.text.trim())
                    }
                }
            }
            ListView {
                id: listView
                preferredHeight: 100
                horizontalAlignment: HorizontalAlignment.Fill
                dataModel: ArrayDataModel {
                    id: model
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ListItemSettingsAdress {preferredHeight: 100}
                    }
                ]
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    var page = adresyNastavenie.createObject()
                    page.adresy = adresy
                    activePane.push(page)
                }
            } // end of listView
        
        
        } // end of container
    } // end of scrollview

}
