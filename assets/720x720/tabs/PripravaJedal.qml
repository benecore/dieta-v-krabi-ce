import bb.cascades 1.2

import "../components"
import "../containers"

TabPage {
    id: root
    
    
    BasePage {
        
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        onCreationCompleted: {
            controlDelegate.sourceComponent = info
        }
        
        attachedObjects: [
            ComponentDefinition {
                id: info
                PripravaJedalInfo{
                
                }
            },
            ComponentDefinition {
                id: fotogaleria
                PripravaJedalFotogaleria{
                
                }
            }
        ]
        
        
        Container {
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            SegmentedControl {
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: "Příprava jídel"
                        value: info
                    },
                    Option {
                        text: "Fotogalerie"
                        value: fotogaleria
                    }
                ]
                onSelectedOptionChanged: {
                    controlDelegate.sourceComponent = selectedValue
                }
            } // end of segment control

            ControlDelegate {
                id: controlDelegate
                delegateActive: true
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
        } // end of root container 
    }
}