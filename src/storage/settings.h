/*
 * settings.h
 *
 *  Created on: 24.2.2014
 *      Author: Benecore
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QObject>
#include <QSettings>
#include <QDebug>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Settings: public bb::cascades::CustomControl
{
	Q_OBJECT
	// General settings
	Q_PROPERTY(bool saveLogin READ saveLogin WRITE setSaveLogin NOTIFY settingsChanged)
public:
	Settings();
	virtual ~Settings();


	static Settings *instance();
	static void destroy();


	inline bool saveLogin() const { return _saveLogin; }
	void setSaveLogin(const bool &value){
		if (_saveLogin != value){
			_saveLogin = value;
			settings->setValue("saveLogin", _saveLogin);
			emit settingsChanged();
		}
	}

signals:
	void settingsChanged();

protected:
	QSettings *settings;


	private:
	Q_DISABLE_COPY(Settings)
	static Settings *_instance;
	bool _saveLogin;
};

#endif /* SETTINGS_H_ */
