import bb.cascades 1.2

Dialog {
    id: root
    
    signal closed()
    
    property variant data: null
    default property alias contentItems: contentContainer.controls
    property variant headerColor: Color.create("#6ebd43")
    property alias headerVisible: freeForm.visible
    property int headerLeftImageSourcePreferredHeight: 80
    property int headerRightImageSourcePreferredHeight: 80
    
    Container {
        preferredHeight: Size.maxHeight
        preferredWidth: Size.maxWidth
        background: Color.create(0.0, 0.0, 0.0, 0.5)
        layout: DockLayout{}
        
        Container {
            id: container
            
            layout: DockLayout{}
            preferredWidth: Size.maxWidth-150
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
            background: Color.create("#f8f8f8")
            
            
            Container {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                
                Container {
                    id: freeForm
                    layout: DockLayout{}
                    background: root.headerColor
                    preferredHeight: 100
                    
                    Container {
                        leftPadding: 25
                        rightPadding: 25
                        horizontalAlignment: HorizontalAlignment.Center
                        verticalAlignment: VerticalAlignment.Center
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        
                        ImageView {
                            id: headerLeftImage
                            visible: true
                            preferredWidth: headerLeftImageSourcePreferredHeight
                            scalingMethod: ScalingMethod.AspectFit
                            verticalAlignment: VerticalAlignment.Center
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: -1
                            }
                            imageSource: "asset:///images/header/hearth-home.png"
                        }
                        
                        Container {
                            objectName: "headerLabelContainer"
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: 1
                            }
                            verticalAlignment: VerticalAlignment.Center
                            leftPadding: !headerLeftImage.visible ? headerLeftImageSourcePreferredHeight : 0
                            rightPadding: !headerRightImage.visible ? headerRightImageSourcePreferredHeight : 0
                            MyLabel {
                                id: headerLabel
                                horizontalAlignment: HorizontalAlignment.Fill
                                textStyle.textAlign: TextAlign.Center
                                fontSize: FontSize.XLarge
                                textStyle.color: Color.White
                                text: data ? data.den : ""
                            }
                        }// end of title container
                        
                        ImageView {
                            id: headerRightImage
                            preferredWidth: headerRightImageSourcePreferredHeight
                            scalingMethod: ScalingMethod.AspectFit
                            verticalAlignment: VerticalAlignment.Center
                            layoutProperties: StackLayoutProperties {
                                spaceQuota: -1
                            }
                            imageSource: activeTab.imageSource
                        }
                    }
                } // end of header
                
                
                Container {
                    id: contentContainer
                    leftPadding: 20
                    rightPadding: 20
                } // end of content
                
                Container {
                    leftPadding: 10
                    rightPadding: 10
                    bottomPadding: 10
                    topPadding: 10
                    horizontalAlignment: HorizontalAlignment.Center
                    Button {
                        text: "Zavřít"
                        onClicked: {
                            root.closed()
                        }
                    }
                } // end of button
            
            } // end of StackLayout container
        }
    } // end of RootContainer

}