import bb.cascades 1.2

ActionItem {
    id: root
    
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    signal clicked
    
    ActionBar.placement: ActionBarPlacement.OnBar
    title: "Přidat"
    imageSource: "asset:///images/actions/add.png"
    onTriggered: {
        root.clicked()
    }
}
