/*
 * cacher.h
 *
 *  Created on: 5.5.2014
 *      Author: benecore
 */

#ifndef CACHER_H_
#define CACHER_H_

#include <QObject>
#include <QDateTime>

class QFile;

class Cacher : public QObject
{
	Q_OBJECT
public:
	Cacher(QObject *parent = 0);
	virtual ~Cacher();


	static Cacher *instance();
	static void destroy();


public slots:
	bool saveCache(const QString &cacheId, const QByteArray &data);
	QByteArray getCache(const QString &cacheId);


private:
	Q_DISABLE_COPY(Cacher)
	static Cacher *_instance;
	QFile *file;

};

#endif /* CACHER_H_ */
