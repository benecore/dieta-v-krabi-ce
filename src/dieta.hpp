/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef DIETA_HPP_
#define DIETA_HPP_

#include <QObject>

#include "dieta.h"
#include "storage/database.h"
#include "storage/settings.h"
#include "custom/sizehelper.h"
#include "custom/invoker.h"
#include "custom/cacher.h"
#include "custom/helper.h"
#include "custom/objednavkaitem.h"
#include "calendar/calendarmodel.h"
#include "descriptions.h"
#include "covers/cover.h"
#include "livecoding/qmlbeam.h"

#include <QtNetwork>

namespace bb
{
namespace cascades
{
class Application;
class LocaleHandler;
class GroupDataModel;

namespace maps {
class MapView;
}
}

namespace platform {
namespace geo {
class GeoLocation;
}
}
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class Dieta : public QObject
{
	Q_OBJECT
	Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)
	Q_PROPERTY(QString facebookMail READ facebookMail WRITE setFacebookMail NOTIFY facebookMailChanged)
public:
	Dieta(bb::cascades::Application *app);
	virtual ~Dieta();


	Q_INVOKABLE
	void addPinToMap(QObject* mapObject, const QString &title, const QString &subtitle);
	Q_INVOKABLE
	void updateMarkers(QObject* mapObject, QObject* containerObject) const;
	Q_INVOKABLE
	inline void blocking(const bool &value){ blockSignals(value); }
	Q_INVOKABLE
	inline QByteArray md5(QByteArray data)
	{
		return QCryptographicHash::hash(data, QCryptographicHash::Md5).toHex();
	}
	Q_INVOKABLE
	void objednat(QObject *objednavkaItem);

	Q_INVOKABLE
	void facebookAccount(const QUrl &accessToken);


	signals:
	void loadingChanged(bool loading);
	void prihlasenieDone(const QByteArray response);
	void registraciaDone(const QByteArray response);
	void mojUcetDone(const QByteArray response);
	void mojeObjednavkyDone(const QByteArray response);
	void mojaObjednavkaDone(const QByteArray response);
	void pridatObjednavkuDone(const QByteArray response);
	void editovatObjednavkuDone(const QByteArray response);
	void ulozitObjednavkuDone(const QByteArray response);
	void odstranitObjednavkuDone(const QByteArray response);
	void regionDone(const QByteArray response);
	void casDoruceniaDone(const QByteArray response);
	void dorucenieDone(const QByteArray response);
	void jedalnicekDone(const QByteArray response);
	void osobnyOdberDone(const QByteArray response);
	void osobnyOdberMestaDone(const QByteArray response);
	void dopravaDone(const QByteArray response);
	void zmenitNastavenieDone(const QByteArray response);
	void pridatAdresuDone(const QByteArray response);
	void editovatAdresuDone(const QByteArray response);
	void odstranitAdresuDone(const QByteArray response);
	void objednatDone(const QByteArray response);
	void facebookDone(const QByteArray response);
	void facebookMailChanged();
	void facebookLoginDone(const QByteArray response);


private slots:
	void onSystemLanguageChanged();


	inline QString facebookMail() const { return _facebookMail; }
	inline void setFacebookMail(const QString &value){
		if (_facebookMail != value){
			_facebookMail = value;
			emit facebookMailChanged();
		}
	}

	QPoint worldToPixel(QObject* mapObject, double latitude, double longitude) const;

	void saveToFile(const QString &filename, const QByteArray &data);

	QByteArray addTimestamp(const QByteArray &data, const int &hours);


	void error(int errorCode, QString errorString);
	void requestReady(const QByteArray response, const QNetworkReply *reply = 0);
	inline bool loading() const { return _loading; }
	inline void setLoading(const bool &value){
		if (_loading != value){
			_loading = value;
			emit loadingChanged(_loading);
		}
	}

	void parsePrihlasenie(const QByteArray response);
	void parseRegistracia(const QByteArray response);
	void parseMojUcet(const QByteArray response);
	void parseMojeObjednavky(const QByteArray response);
	void parseMojaObjednavka(const QByteArray response);
	void parsePridatObjednavku(const QByteArray response);
	void parseEditovatObjednavku(const QByteArray response);
	void parseUlozitObjednavku(const QByteArray response);
	void parseOdstranitObjednavku(const QByteArray response);
	void parseRegion(const QByteArray response);
	void parseCasDorucenia(const QByteArray response);
	void parseDorucenie(const QByteArray response);
	void parseJedalnicek(const QByteArray response);
	void parseOsobnyOdber(const QByteArray response);
	void parseOsobnyOdberMesta(const QByteArray response);
	void parseDoprava(const QByteArray response);
	void parseZmenitNastavenie(const QByteArray response);
	void parsePridatAdresu(const QByteArray response);
	void parseEditovatAdresu(const QByteArray response);
	void parseOdstranitAdresu(const QByteArray response);
	void parseObjednat(const QByteArray response);
	void parseFacebookLogin(const QByteArray response);


	void finished(QNetworkReply *reply);

private:
	QTranslator* m_pTranslator;
	bb::cascades::LocaleHandler* m_pLocaleHandler;
	/*******************************************/
	bool _loading;
	DietaLib *api;
	CalendarModel *calendarModel;
	Descriptions *desc;
	QNetworkAccessManager *manager;
	QString _accessTokenFacebook;
	QString _facebookMail;
};

#endif /* DIETA_HPP_ */
