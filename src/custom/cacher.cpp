/*
 * cacher.cpp
 *
 *  Created on: 5.5.2014
 *      Author: benecore
 */

#include "cacher.h"
#include <QDebug>
#include <QFile>
#include <QDir>

const char *const CACHE_PATH = "data/cache_data";

Cacher *Cacher::_instance = 0;

Cacher::Cacher(QObject *parent) :
	QObject(parent),
	file(0)
{
	QDir dir(CACHE_PATH);
	if (!dir.exists()){
		Q_ASSERT(QDir().mkpath(CACHE_PATH));
	}
}

Cacher::~Cacher()
{
	if (file){
		delete file;
		file = 0;
	}
}


Cacher *Cacher::instance()
{
	if (!_instance)
		_instance = new Cacher;
	return _instance;
}


void Cacher::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}


bool Cacher::saveCache(const QString& cacheId, const QByteArray& data)
{
	file = new QFile(QString("%1/%2").arg(CACHE_PATH).arg(cacheId));
	if (!file->open(QIODevice::WriteOnly)){
		qDebug() << "Unable write to file" << file->errorString();
		return false;
	}
	file->write(data);
	file->close();
	file->deleteLater();
	return true;
}

QByteArray Cacher::getCache(const QString& cacheId)
{
	file = new QFile(QString("%1/%2").arg(CACHE_PATH).arg(cacheId));
	if (!file->open(QIODevice::ReadOnly)){
		qDebug() << "Unable to read file" << file->errorString();
		return QByteArray();
	}
	QByteArray data = file->readAll();
	file->close();
	file->deleteLater();
	return data;
}
