#ifndef HELPER_H
#define HELPER_H

#include <QObject>
#include <bb/PpsObject>
#include <bb/cascades/Application>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Helper : public bb::cascades::CustomControl
{
    Q_OBJECT
public:
    explicit Helper();
    virtual ~Helper();


    static Helper *instance();
    static void destroy();

    Q_INVOKABLE
    QString getMonthName(const int& month = 0) const;
    Q_INVOKABLE
    QDateTime getDateObject(const int &startHour = 0) const;
    Q_INVOKABLE
    QString getDate(const int &day = 0, const QVariant &format = QVariant());
    Q_INVOKABLE
    int getTimestamp(const int &hours = 0);
    Q_INVOKABLE
    inline QByteArray encodeObject(const QVariantMap &ppsData, bool *ok=0){
    	return bb::PpsObject::encode(ppsData, ok);
    }


private:
    Q_DISABLE_COPY(Helper)
	static Helper *_instance;
};

#endif // HELPER_H
