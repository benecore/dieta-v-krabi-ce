import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../actions"


BasePage {
    id: root
    
    function osobnyOdber(response){
        var json = JSON.parse(response)
        pobockaDropdown.removeAll()
        for (var item in json.data){
            if (mestaDropdown.selectedValue == 1){
                if (json.data[item].mesta_id == 1){
                    console.log("MESTO: ".concat(json.data[item].nazev))
                    var obj = dynamicOption.createObject()
                    obj.text = json.data[item].nazev + " | " + json.data[item].adresa
                    obj.value = json.data[item].id
                    if (json.data[item].id == loadedData.pobocky_id){
                        obj.selected = true
                    }
                    pobockaDropdown.add(obj)
                }
            }else{
                if (json.data[item].mesta_id == 2){
                    console.log("MESTO: ".concat(json.data[item].nazev))
                    var obj = dynamicOption.createObject()
                    obj.text = json.data[item].nazev + " | " + json.data[item].adresa
                    obj.value = json.data[item].id
                    if (json.data[item].id == loadedData.pobocky_id){
                        obj.selected = true
                    }
                    pobockaDropdown.add(obj)
                }
            }
        }
    }
    
    
    function osobnyOdberMesta(response){
        mestaDropdown.removeAll()
        var json = JSON.parse(response)
        for (var item in json.data){
            var obj = dynamicOption.createObject(mestaDropdown)
            obj.text = json.data[item].nazev
            obj.value = json.data[item].id
            mestaDropdown.add(obj)
        }
        mestaDropdown.selectedIndex = --loadedData.mesta_id
    }
    
    
    function region(response){
        var json = JSON.parse(response)
        setCas(json.region)
    }
    
    function setCas(region){
        casy.removeAll()
        var list = []
        for (var item in root.dataCasy){
            var cas = root.dataCasy[item]
            if (region != 0){
                if (cas.region == region){
                    list.push(cas)
                }
            }
        }
        for (var item in list){
            console.log("CAS: ".concat(JSON.stringify(list[item])))
            var option = dynamicOption.createObject(casy)
            option.text = list[item].nazev
            option.value = list[item].id
            if (data.data.edit == 1 && list[item].id == data.data.cas_doruceni_id){
                option.selected = true
            }
            casy.add(option)
        }
        if (casy.options.length == 0 && adresy.selectedOption != null && dorucenie.selectedValue == 2){
            notCasy.visible = true
        }else{
            notCasy.visible = false
        }
    }
    
    
    function pocetJedalModel(select, pocet){
        var i = 1;
        while(i != kredit+1){
            var option = dynamicOption.createObject(pocetJedal)
            option.text = i
            option.value = i
            if (select && i == pocet){
                option.selected = select
            }
            pocetJedal.add(option)
            ++i;
        }
    }
    
    
    function editovatObjednavkuDone(response){
        var json = JSON.parse(response)
        if (json){
            root.dataCasy = json.casy
            var item = json.data[0]
            loadedData = item
            setCas(item.region)
            pocetJedalModel(true, item.pocet_jidel)
            dorucenie.selectedIndex = --item.zpusob_doruceni_id
            poznamka.text = item.poznamka
        }
    }
    
    
    function pridatObjednavkuDone(response){
        var json = JSON.parse(response)
        if (json){
            root.dataCasy = json.casy
            var item = json.data[0]
            loadedData = item
            setCas(item.region)
            pocetJedalModel(true, item.pocet_jidel)
            dorucenie.selectedIndex = --item.zpusob_doruceni_id
            poznamka.text = item.poznamka
        }
    }
    
    
    function mojUcetDone(response){
        adresy.removeAll()
        var json = JSON.parse(response);
        for (var item in json.data){
            var option = dynamicOption.createObject(adresy)
            option.text = json.data[item].adresa
            option.value = json.data[item]
            if (data.data.edit == 1 && option.value.id == data.data.adresy_id){
                option.selected = true
            }
            adresy.add(option)
        }
        var obj = dynamicOption.createObject(adresy)
        obj.text = "Vytvořit novou adresu"
        obj.value = "adresa"
        adresy.add(obj)
    }
    
    
    function ulozitObjednavkuDone(response){
        var json = JSON.parse(response)
        if (json.error == 0){
            Api.mojaObjednavka(objednavkaId)
            root.done(json.data)
        }else{
            root.error(json.data)         
        }
    }
    
    
    function odstranitObjednavkuDone(response){
        var json = JSON.parse(response)
        if (json.error == 0){
            Api.mojaObjednavka(objednavkaId)
            root.removed(json.data)
        }else{
            root.error(json.data)         
        }
    }
    
    
    signal done(string text)
    signal error(string text)
    signal removed(string text)
    
    
    property string objednavkaId: null
    property string datum
    property int kredit
    property variant data: null
    property int vycerpane
    property variant dataCasy: null
    property bool edit: false
    property alias title: titleHeader.headerText
    property variant loadedData: null
    header: TitleHeader {
        id: titleHeader
        headerText: datum
        headerRightImage: true
        headerRightImageSource: "asset:///images/tabs/objednavka.png"
    }
    backgroundColor: Color.create("#f8f8f8")
    
    
    onCreationCompleted: {
        Api.loading = true
        App.osobnyOdberDone.connect(osobnyOdber)
        App.osobnyOdberMestaDone.connect(osobnyOdberMesta)
        App.mojUcetDone.connect(mojUcetDone)
        App.regionDone.connect(region)
        App.editovatObjednavkuDone.connect(editovatObjednavkuDone)
        App.pridatObjednavkuDone.connect(pridatObjednavkuDone)
        App.ulozitObjednavkuDone.connect(ulozitObjednavkuDone)
        App.odstranitObjednavkuDone.connect(odstranitObjednavkuDone)
    
    }
    
    
    onActiveChanged: {
        if (active){
            var mojucet = Cacher.getCache("moj_ucet")
            if (mojucet == ""){
                console.log("MOJ UCET - ONLINE")
                Api.mojUcet()
            }else{
                console.log("MOJ UCET - OFFLINE")
                mojUcetDone(mojucet)
            }
            if (edit){
                Api.editovatObjednavku(objednavkaId, datum)
            }else{
                Api.pridatObjednavku(objednavkaId, datum)
            }
        }
    }
    
    actions: [
        ObjednatAction {
            title: root.edit ? "Uložit" : "Přidat"
            onClicked: {
                if (adresy.selectedValue == "adresa"){
                    Api.ulozitObjednavku(datum, objednavkaId, !root.edit ? 0 : data.data.id, dorucenie.selectedValue == 2 ? 0 : mestaDropdown.selectedValue,
                    dorucenie.selectedValue == 2 ? 0 : pobockaDropdown.selectedValue, dorucenie.selectedValue == 1 ? 0 : casy.selectedValue, dorucenie.selectedValue,
                    pocetJedal.selectedValue, -1, poznamka.text.trim(),
                    menoField.text.trim(), uliceField.text.trim(), mestoField.text.trim(), pscField.text.trim())
                }else{
                    Api.ulozitObjednavku(datum, objednavkaId, !root.edit ? 0 : data.data.id, dorucenie.selectedValue == 2 ? 0 : mestaDropdown.selectedValue,
                    dorucenie.selectedValue == 2 ? 0 : pobockaDropdown.selectedValue, dorucenie.selectedValue == 1 ? 0 : casy.selectedValue, dorucenie.selectedValue,
                    pocetJedal.selectedValue, dorucenie.selectedValue == 1 ? 0 : adresy.selectedValue.id, poznamka.text.trim())
                }
            }
        },
        RemoveAction {
            onClicked: {
                dialog.show()
            }
        }
    ]
    
    
    attachedObjects: [
        ComponentDefinition {
            id: dynamicOption
            Option {}
        },
        SystemDialog {
            id: dialog
            title: "Potvrzení"
            body: "Opravdu chcete odstránit jídlo na "+datum+" ?"
            confirmButton.label: "Odstránit"
            cancelButton.label: "Zrušit"
            onFinished: {
                if (value == SystemUiResult.ConfirmButtonSelection){
                    console.log("Objednavka id: "+objednavkaId+" | dodavka id: "+loadedData.id)
                    Api.odstranitObjednavku(objednavkaId, loadedData.id)
                }
            }
        }
    ]
    
    
    ActivityIndicator {
        preferredHeight: 200
        preferredWidth: 200
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        visible: Api.loading
        running: visible
    }
    
    
    ScrollView {
        opacity: Api.loading ? 0 : 1
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties{
            scrollMode: ScrollMode.Vertical
            pinchToZoomEnabled: false
        }
        
        Container {
            leftPadding: 20
            topPadding: 20
            rightPadding: 20
            bottomPadding: 20
            
            
            DropDown {
                id: pocetJedal
                title: "Počet jídel"
            }// end of Pocet Jedal
            
            Divider{}
            
            DropDown {
                id: dorucenie
                title: "Způsob doručení"
                options: [
                    Option {
                        text: "Osobní odběr"
                        value: 1
                    },
                    Option {
                        text: "Dovoz"
                        value: 2
                    }
                ]
                onSelectedOptionChanged: {
                    if (selectedValue == 1){
                        var result = Cacher.getCache("osobny_odber_mesta")
                        if (result == ""){
                            console.log("OSOBNY ODBER MESTA - ONLINE")
                            Api.osobnyOdberMesta()
                        }else{
                            console.log("OSOBNY ODBER MESTA - OFFLINE")
                            osobnyOdberMesta(result)
                        }
                        
                    }
                }
            }// end of dorucenie
            
            Divider{}
            
            Container {
                // DOVOZ container
                horizontalAlignment: HorizontalAlignment.Fill
                
                DropDown {
                    id: adresy
                    visible: dorucenie.selectedOption != null && dorucenie.selectedValue == 2
                    title: "Doručovací adresa"
                    onSelectedOptionChanged: {
                        console.log("ADRESA OPTION CHANGED")
                        if (selectedValue == "adresa"){
                            novaAdresa.visible = true
                        }else{
                            novaAdresa.visible = false
                            Api.region(selectedValue.psc.trim())
                        }
                    }
                }
                
                MyLabel {
                    id: notCasy
                    visible: adresy.selectedOption == null && dorucenie.selectedValue == 2
                    multiline: true
                    fontFamily: "RobotoLightItalic"
                    fontSize: FontSize.Small
                    textStyle.color: Color.Red
                    text: "Nejdříve si vyberte Vaši doručovací adresu nebo vložte novou doručovací adresu. Čas doručení je závislý na Vašem PSČ"
                }
                
                
                Container {
                    id: novaAdresa
                    visible: dorucenie.selectedOption != null && dorucenie.selectedValue == 2 && adresy.selectedValue == "adresa"
                    
                    TextField {
                        id: menoField
                        hintText: "Jméno/Firma*"
                        inputMode: TextFieldInputMode.Text
                        input.submitKey: SubmitKey.Next
                        input.onSubmitted: {
                            uliceField.requestFocus()
                        }
                    }
                    TextField {
                        id: uliceField
                        hintText: "Ulice a č.p.*"
                        inputMode: TextFieldInputMode.Text
                        input.submitKey: SubmitKey.Next
                        input.onSubmitted: {
                            mestoField.requestFocus()
                        }
                    }
                    TextField {
                        id: mestoField
                        hintText: "Město*"
                        inputMode: TextFieldInputMode.Text
                        input.submitKey: SubmitKey.Next
                        input.onSubmitted: {
                            pscField.requestFocus()
                        }
                    }
                    TextField {
                        id: pscField
                        hintText: "PSČ*"
                        inputMode: TextFieldInputMode.Text
                        input.submitKey: SubmitKey.Submit
                        input.onSubmitted: {
                            if (text.length){
                                Api.region(text.trim())
                            }
                        }
                    }
                }
                
                DropDown {
                    id: casy
                    visible: dorucenie.selectedOption != null && dorucenie.selectedValue == 2
                    title: "Čas doručení*"
                }
            } // end of Dovoz
            
            Container {
                visible: dorucenie.selectedOption != null && dorucenie.selectedValue == 1
                horizontalAlignment: HorizontalAlignment.Fill
                
                DropDown {
                    id: mestaDropdown
                    visible: (dorucenie.selectedValue == 1)
                    title: "Místo odběru*"
                    onSelectedValueChanged: {
                        var result = Cacher.getCache("osobny_odber")
                        if (result == ""){
                            console.log("OSOBNY ODBER - ONLINE")
                            Api.osobnyOdber()
                        }else{
                            console.log("OSOBNY ODBER - OFFLINE")
                            osobnyOdber(result)
                        }
                    }
                } // end of mesta dropdown
                
                DropDown {
                    id: pobockaDropdown
                    enabled: (mestaDropdown.selectedOption != 0)
                    visible: (dorucenie.selectedValue == 1)
                    title: "Pobočka*"
                } // end of picker
            } // end of MestoOdberu
            
            TextField {
                id: poznamka
                inputMode: TextFieldInputMode.Text
                hintText: "poznámka"
            }
        }
    
    } // end of scrollView

}