/*
 * calendarmodel.h
 *
 *  Created on: 22.4.2014
 *      Author: benecore
 */

#ifndef CALENDARMODEL_H_
#define CALENDARMODEL_H_

#include <bb/cascades/LocaleHandler>
#include <bb/cascades/DataModel>
#include <bb/cascades/GroupDataModel>
#include <QDateTime>
#include <QLocale>

#include "calendaritem.h"

using namespace bb::cascades;

class CalendarModel : public bb::cascades::GroupDataModel {
	Q_OBJECT
	Q_ENUMS(QLocale::Language)
	Q_PROPERTY(QLocale::Language language READ language WRITE setLanguage NOTIFY propertiesChanged)
	Q_PROPERTY(QString monthName READ monthName NOTIFY propertiesChanged)
	Q_PROPERTY(QString year READ year NOTIFY propertiesChanged)
public:
	CalendarModel();
	virtual ~CalendarModel();

	Q_INVOKABLE
	void generateModel(int months = 0);
	Q_INVOKABLE
	void resetModel();
	Q_INVOKABLE
	void updateModel(const QVariantList &data);
	Q_INVOKABLE
	int todayIndex();


private slots:
	bool canEdit(const QDate &date);
	inline QLocale::Language language() const { return _language; }
	inline void setLanguage(const QLocale::Language &value){
		if (_language != value){
			_language = value;
			emit propertiesChanged();
		}
	}
	inline QString year() const { return currentDate.toString("yyyy"); }
	inline QString monthName() const {
		QString monthName = QLocale(_language).standaloneMonthName(currentDate.month());
		monthName = monthName.left(1).toUpper()+monthName.mid(1);
		return monthName;
	}
	void onSystemLanguageChanged();

protected:
	inline QString localizedDayName(const int &day, const QLocale::FormatType &format = QLocale::LongFormat){
		QString dayName = QLocale(_language).standaloneDayName(day, format);
		dayName = dayName.left(1).toUpper()+dayName.mid(1);
		return dayName;
	}


signals:
	void propertiesChanged();


private:
	QLocale locale;
	QDate currentDate;
	QLocale::Language _language;
	LocaleHandler *_localeHandler;
	int scrollIndex;
};

#endif /* CALENDARMODEL_H_ */
