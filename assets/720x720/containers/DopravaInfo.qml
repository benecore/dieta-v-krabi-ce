import bb.cascades 1.2

import "../components"


ScrollView {
    id: root
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        topPadding: 10
        leftPadding: 10
        rightPadding: 10
        bottomPadding: 10
        MyLabel {
            multiline: true
            fontSize: FontSize.Medium
            fontFamily: "RobotoRegular"
            text: Desc.dopravaText
        }
    }
}