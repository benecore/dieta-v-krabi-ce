import bb.cascades 1.2

ActionItem {
    id: root
    
    
    property alias text: root.title
    property alias icon: root.imageSource
    
    signal clicked
    
    ActionBar.placement: ActionBarPlacement.OnBar
    title: "Odstránit"
    imageSource: "asset:///images/actions/delete.png"
    onTriggered: {
        root.clicked()
    }
}