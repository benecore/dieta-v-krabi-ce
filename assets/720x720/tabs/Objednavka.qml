import bb.cascades 1.2
import bb.system 1.2
import bb.cascades.pickers 1.0
import com.devpda.items 1.0

import "../components"
import "../actions"
import "../pages"
import "../items"


TabPage {
    id: root
    
    attachedObjects: [
        ObjednavkaItem {
            id: objednavkaItem
        }
    ]
    
    BasePage {
        id: page
        
        function canContinue(){
            if (dietaGroup.selectedOption && proGroup.selectedOption && dlzkaGroup.selectedOption
            && datePicker.value.getDate() >= Helper.getDate(4, "dd") 
            && datePicker.value.getMonth() >= Helper.getDate(0, "M")-1
            && datePicker.value.getDay() != 0){
                allOK = true
            }else{
                allOK = false
            }
        }
        
        
        
        
        property bool allOK: false
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource           
        }
        backgroundColor: Color.create("#f8f8f8")
        
        onCreationCompleted: {
            dietaGroup.selectedOptionChanged.connect(canContinue)
            proGroup.selectedOptionChanged.connect(canContinue)
            dlzkaGroup.selectedOptionChanged.connect(canContinue)
            datePicker.valueChanged.connect(canContinue)
        }
        
        actions: [
            ActionItem {
                enabled: page.allOK
                ActionBar.placement: ActionBarPlacement.OnBar
                title: "DORUČENÍ"
                imageSource: "asset:///images/actions/next.png"
                onTriggered: {
                    var page = objednavka2.createObject()
                    page.objednavkaItem = objednavkaItem
                    activePane.push(page)
                }
            }
        ]
        
        attachedObjects: [
            ComponentDefinition {
                id: objednavka2
                Objednavka2 {
                    
                }
            }
        ]
        
        
        Container {
            topPadding: 10
            leftPadding: 10
            rightPadding: 10
            bottomPadding: 10
            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                imageSource: "asset:///images/objednavka1.png"
            }
            
            ScrollView {
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                Container {
                    
                    Header {
                        title: "VÁŠ PROGRAM"
                    }
                    
                    MyLabel {
                        fontFamily: "RobotoBold"
                        fontSize: FontSize.Medium
                        text: "DIETA*"
                    }
                    
                    RadioGroup {
                        id: dietaGroup
                        topMargin: 0
                        bottomMargin: 0
                        leftMargin: 0
                        rightMargin: 0
                        options: [
                            Option {
                                text: "HUBNU"
                                value: 1
                            },
                            Option {
                                text: "ŽIJU ZDRAVĚ"
                                value: 2
                            }
                        ]
                        onSelectedOptionChanged: {
                            objednavkaItem.dietaID = selectedValue
                        }
                    }
                    
                    MyLabel {
                        fontFamily: "RobotoBold"
                        fontSize: FontSize.Medium
                        text: "PRO*"
                    }
                    
                    RadioGroup {
                        id: proGroup
                        topMargin: 0
                        bottomMargin: 0
                        leftMargin: 0
                        rightMargin: 0
                        options: [
                            Option {
                                text: "ŽENA"
                                value: 1
                            },
                            Option {
                                text: "MUŽ"
                                value: 2
                            }
                        ]
                        onSelectedOptionChanged: {
                            objednavkaItem.preID = selectedValue
                        }
                    }
                    
                    MyLabel {
                        fontFamily: "RobotoBold"
                        fontSize: FontSize.Medium
                        text: "Počáteční den konzumace"
                    }
                    
                    DateTimePicker {
                        id: datePicker
                        mode: DateTimePickerMode.Date
                        value: Helper.getDate(4)
                        onValueChanged: {
                            objednavkaItem.datum = Qt.formatDate(datePicker.value,"dd.MM.yyyy")
                            if (value.getTime() > new Date().getTime()){
                                if (value.getDate() < Helper.getDate(4, "dd") && value.getMonth() == Helper.getDate(0, "M")-1){
                                    toast.body = "Počáteční den musí být minimálně ".concat(Helper.getDate(4, "dd.MM.yyyy"))
                                    toast.show()
                                }else if (value.getDay() == 0){
                                    toast.body = "Neděle nelze označit jako počáteční den konzumace"
                                    toast.show()
                                }
                            }else{
                                toast.body = "Počáteční den musí být minimálně ".concat(Helper.getDate(4, "dd.MM.yyyy"))
                                toast.show()
                            }
                        }
                        attachedObjects: [
                            SystemToast {
                                id: toast
                                button.label: ""
                                autoUpdateEnabled: true
                            }
                        ]
                    } // end of Date picker
                    
                    RadioGroup {
                        id: dlzkaGroup
                        options: [
                            Option {
                                text: "zkušební den"
                                value: 8
                            },
                            Option {
                                text: "zkušební týden"
                                value: 1
                            },
                            Option {
                                text: "měsíc bez sobot"
                                value: 2
                            },
                            Option {
                                text: "měsíc se sobotami"
                                value: 3
                            },
                            Option {
                                text: "2 měsíce bez sobot"
                                value: 4
                            },
                            Option {
                                text: "2 měsíce se sobotami"
                                value: 5
                            }
                        ]
                        onSelectedOptionChanged: {
                            console.log("SELECTED VALUE: "+selectedValue)
                            objednavkaItem.dlzkaProgramuID = selectedValue
                        }
                    } // end of dlzka group
                }
            }
        }
    }
}