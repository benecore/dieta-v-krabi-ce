/*
 * cover.h
 *
 *  Created on: 8.5.2014
 *      Author: benecore
 */

#ifndef COVER_H_
#define COVER_H_

#include <bb/cascades/SceneCover>
#include <bb/cascades/Container>
#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>

using namespace bb::cascades;

class Cover : public SceneCover
{
	Q_OBJECT
public:
	Cover(QObject *parent = 0);
	virtual ~Cover();


private:
	Q_DISABLE_COPY(Cover)
	Container *_container;
	QmlDocument *qml;
};

#endif /* COVER_H_ */
