import bb.cascades 1.2

ListItemBase {
    id: root
    
    property alias scale: image.scalingMethod
    
    leftPadding: 2
    rightPadding: 2
    topPadding: 2
    bottomPadding: 2
    
    ImageView {
        id: image
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scalingMethod: ScalingMethod.AspectFill
        imageSource: ListItemData
    }

}