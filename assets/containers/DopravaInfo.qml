import bb.cascades 1.2

import "../components"


ScrollView {
    id: root
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        MyLabel {
            multiline: true
            fontSize: FontSize.Medium
            fontFamily: "RobotoRegular"
            text: Desc.dopravaText
        }
    }
}