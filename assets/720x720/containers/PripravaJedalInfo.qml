import bb.cascades 1.2

import "../components"

ScrollView {
    id: root
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        MyLabel {
            horizontalAlignment: HorizontalAlignment.Center
            fontFamily: "RobotoRegular"
            fontSize: FontSize.Large
            text: "<html><b>KDE</b> se vaše jídlo připravuje</html>"
        }
        MyLabel {
            horizontalAlignment: HorizontalAlignment.Fill
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            multiline: true
            //text: "<html>Naše krabičková dieta je připravována výhradně <b>z čerstvých a kvalitních surovin pod přísným hygienickým dohledem.</b><br/><br/>Všechna jídla jsou vyvíjená <b>profesionálním týmem kuchařů</b> s bohatými zkušenostmi s domácí i mezinárodní kuchyní.<br/><br/>Pro přípravu krabičkové diety využíváme <b>nejmodernějších přístrojů</b>. Veškeré vybavení naší kuchyně splňuje ty nejnáročnější požadavky moderního a zdravého zařízení.<br/><br/>Naše výrobna je <b>držitelem certifikátu HACCP</b>. Tento certifikát zaručuje bezpečnost a především vysokou kvalitu našich programů ve všech fázích manipulace s pokrmy, surovinami, skladováním a distribucí.<br/><br/>Jako jediná společnost na trhu krabičkových diet se v současné době certifikujeme na <b>ISO normu 22000</b>, což je nejvyšší možná mezinárodní norma, která specifikuje požadavky na systém bezpečnosti potravin pro všechny organizace v potravinovém řetězci, které musí být naplněny, aby byla zajištěna bezpečnost potravin.</html>"
            text: Desc.pripravaJedalText
        }
    }
}