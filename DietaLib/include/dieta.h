#ifndef DIETALIB_H
#define DIETALIB_H

#include "dieta_global.h"
#include <QObject>

class QNetworkReply;
class DietaLibPrivate;
class DIETASHARED_EXPORT DietaLib : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool loading READ loading WRITE setLoading NOTIFY loadingChanged)
    Q_PROPERTY(bool synchronous READ synchronous WRITE setSynchronous NOTIFY synchronousChanged)
public:
    DietaLib(QObject *parent = 0);
    virtual ~DietaLib();

    enum Request{
        REGION,
        CAS_DORUCENIA,
        DORUCENIE,
        JEDALNICEK,
        OSOBNY_ODBER,
        OSOBNY_ODBER_MESTA,
        DOPRAVA,
        PRIHLASENIE,
        FACEBOOK_LOGIN,
        REGISTRACIA,
        MOJ_UCET,
        ZMENIT_NASTAVENIE,
        ADRESA_PRIDANIE,
        ADRESA_EDITACIA,
        ADRESA_ODSTRANENIE,
        OBJEDNAVKY,
        MOJA_OBJEDNAVKA,
        PRIDAT_OBJEDNAVKU,
        EDITOVAT_OBJEDNAVKU,
        ULOZIT_OBJEDNAVKU,
        ODSTRANIT_OBJEDNAVKU,
        OBJEDNAT
    };

    enum DietaError{
        NoError = 0,
        Error = 1
    };


    Q_INVOKABLE
    void osobnyOdberMesta();
    Q_INVOKABLE
    void region(const QString &psc);
    Q_INVOKABLE
    void casDorucenia();
    Q_INVOKABLE
    void dorucenie();
    //typ = 0 - tento tyzden | 1 - nasledujuci tyzden | 2 - minuly tyzden
    Q_INVOKABLE
    void jedalnicek(const int &typ = 0);
    Q_INVOKABLE
    void osobnyOdber();
    Q_INVOKABLE
    void doprava(const QString &psc);
    Q_INVOKABLE
    void prihlasenie(const QString &email, const QString &password);
    Q_INVOKABLE
    void prihlasenieFacebook(const QString &email, const QString &id, const QString meno, const QString priezvisko, const QString &accessToken);
    Q_INVOKABLE
    void registrovat(const QString &email, const QString &meno, const QString &priezvisko, const QString &telefon);
    Q_INVOKABLE
    void mojUcet();
    Q_INVOKABLE
    void zmenitNastavenie(const QString &meno, const QString &priezvisko, const QString &telefon);
    Q_INVOKABLE
    void pridatAdresu(const QString &meno, const QString &ulica, const QString &obec, const QString &psc);
    Q_INVOKABLE
    void editovatAdresu(const QString &id, const QString &meno, const QString &ulica, const QString &obec, const QString &psc);
    Q_INVOKABLE
    void odstranitAdresu(const QString &id);
    Q_INVOKABLE
    void objednavky();
    Q_INVOKABLE
    void mojaObjednavka(const QString &id);
    Q_INVOKABLE
    void pridatObjednavku(const QString &id, const QString &datum /* ISO format 24.04.2014 */);
    Q_INVOKABLE
    void editovatObjednavku(const QString &id, const QString &datum /* ISO format 24.04.2014 */);
    Q_INVOKABLE
    void ulozitObjednavku(const QString &datum /* ISO format 24.04.2014 */,
                          const QString &idObjednavky,
                          const QString &idDodavky,
                          const QString &idMesta,
                          const QString &idPobocky,
                          const QString &idCasDorucenia,
                          const QString &idSposobDorucenia,
                          const QString &pocetJedal,
                          const QString &idAdresy,
                          const QString &poznamka ="",
                          const QString &meno = "",
                          const QString &ulica = "",
                          const QString &obec = "",
                          const QString &psc = "");
    Q_INVOKABLE
    void odstranitObjednavku(const QString &idObjednavky, const QString &idDodavky);
    Q_INVOKABLE
    void objednat(const QString &dieta, const QString &preKoho, const QString &datum /*01.01.2011*/, const QString &dlzkaProgramu,
                  const QString &dorucenie, const QString &mesto, const QString &pobocka, const QString &casDorucenia,
                  const QString &meno, const QString &priezvisko, const QString &telefon, const QString &email,
                  const QString &faMeno, const QString &faUlica, const QString &faObec, const QString &faPsc, const QString &faIc, const QString &faDic,
                  const QString &rovnakaAdresa /*0 = neni rovnaka | 1 = je rovnaka*/,
                  const QString &doMeno, const QString &doUlica, const QString &doObec, const QString &doPsc,
                  const QString &voucher /*0 = nemam | 1 = mam*/, const QString &voucherKod, const QString &poznamka);


    Request request() const;
    DietaError error() const;
    int errorCode() const;
    QString errorString() const;

    bool debugEnabled() const;
    void setDebugEnabled(bool &debugEnabled);

    bool synchronous() const;
    void setSynchronous(const bool &value);
    Q_INVOKABLE
    void setLoginDetails(const QString &mail, const QString &password);

signals:
    void requestReady(const QByteArray networkReply, const QNetworkReply *reply = 0);
    void error(int errorCode, QString errorString);
    void loadingChanged(bool loading);
    void synchronousChanged(bool synchronous);


private slots:
    bool loading() const;
    void setLoading(const bool &value);
    void requestFinished(QNetworkReply *reply);


private:
    DietaLibPrivate *const d_ptr;
    Q_DISABLE_COPY(DietaLib)
    Q_DECLARE_PRIVATE(DietaLib)
};

#endif // DIETALIB_H
