/*
 * Copyright (c) 2011-2013 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "dieta.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/data/JsonDataAccess>
#include <bb/cascades/Container>
#include <QDebug>

#include <bb/cascades/maps/MapView>
#include <bb/cascades/maps/MapData>
#include <bb/cascades/maps/DataProvider>
#include <bb/platform/geo/Point>
#include <bb/platform/geo/GeoLocation>
#include <bb/platform/geo/Marker>
#include <bb/UIToolkitSupport>


using namespace bb;
using namespace bb::cascades;
using namespace bb::data;
using namespace bb::cascades::maps;
using namespace bb::platform::geo;

Dieta::Dieta(bb::cascades::Application *app) :
								QObject(app),
								_loading(false)
{
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf8"));

	manager = new QNetworkAccessManager(this);
	connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));
	api = new DietaLib(this);
	connect(api, SIGNAL(requestReady(QByteArray,const QNetworkReply*)),
			this,
			SLOT(requestReady(QByteArray,const QNetworkReply*)));
	connect(api, SIGNAL(error(int,QString)),
			this,
			SLOT(error(int,QString)));
	api->osobnyOdber();
	api->osobnyOdberMesta();
	// set login if is saved
	QVariantMap loginDetails = Database::instance()->getLogin();
	if (!loginDetails.value("email").isNull() || !loginDetails.value("password").isNull()){
		api->setLoginDetails(loginDetails.value("email").toString(),
				loginDetails.value("password").toString());
		api->mojUcet();
	}
	calendarModel = new CalendarModel;
	desc = new Descriptions;
	// prepare the localization
	m_pTranslator = new QTranslator(this);
	m_pLocaleHandler = new LocaleHandler(this);

	//bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	//Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	//Q_UNUSED(res);

	// initial load
	//onSystemLanguageChanged();

	qmlRegisterType<CalendarModel>("com.devpda.models", 1, 0, "CalendarModel");
	qmlRegisterType<Invoker>("com.devpda.tools", 1, 0, "Invoker");
	qmlRegisterType<ObjednavkaItem>("com.devpda.items", 1, 0, "ObjednavkaItem");
	// Create scene document from main.qml asset, the parent is set
	// to ensure the document gets destroyed properly at shut down.
	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this)
    																		.property("App", this)
    																		.property("Api", api)
    																		.property("Size", SizeHelper::instance())
    																		.property("DB", Database::instance())
    																		.property("Settings", Settings::instance())
    																		.property("CalendarModel", calendarModel)
    																		.property("Desc", desc)
    																		.property("Helper", Helper::instance())
    																		.property("Cacher", Cacher::instance());

	QString workDir = QDir::currentPath();
	QDeclarativePropertyMap* dirPaths = new QDeclarativePropertyMap(this);
	dirPaths->insert("images", QVariant(QString(
			"file://" + workDir + "/app/native/assets/images/")));
	qml->setContextProperty("Paths", dirPaths);


	// Create root object for the UI
	AbstractPane *root = qml->createRootObject<AbstractPane>();


	// Cover
	app->setCover(new Cover(this));
	// Set created root object as the application scene
	app->setScene(root);

	new QmlBeam(this);
}


Dieta::~Dieta()
{
	delete manager;
	delete api;
	delete calendarModel;
	SizeHelper::destroy();
	Database::destroy();
	Settings::destroy();
	delete desc;
	Cacher::destroy();
	Helper::destroy();
}


void Dieta::onSystemLanguageChanged()
{
	QCoreApplication::instance()->removeTranslator(m_pTranslator);
	// Initiate, load and install the application translation files.
	QString locale_string = QLocale().name();
	QString file_name = QString("Dietka_%1").arg(locale_string);
	if (m_pTranslator->load(file_name, "app/native/qm")) {
		QCoreApplication::instance()->installTranslator(m_pTranslator);
	}
}



void Dieta::facebookAccount(const QUrl& accessToken)
{
	QString result = accessToken.toString();
	result = result.replace("#", "?");
	QUrl newUrl(result);
	_accessTokenFacebook = newUrl.queryItemValue("access_token");

	qDebug() << "Access token:" << _accessTokenFacebook;

	QString tokenQuery = newUrl.queryItemValue("access_token");
	QUrl requestUrl("https://graph.facebook.com/me");
	requestUrl.addQueryItem("access_token", tokenQuery);

	QNetworkRequest req(requestUrl);

	manager->get(req);
}




void Dieta::addPinToMap(QObject* mapObject, const QString &title, const QString &subtitle)
{
	MapView* mapView = qobject_cast<MapView*>(mapObject);
	DataProvider* deviceLocDataProv = new DataProvider("device-location-data-provider");
	mapView->mapData()->addProvider(deviceLocDataProv);
	qDebug() << Q_FUNC_INFO << mapView;
	GeoLocation* newDrop = new GeoLocation();
	newDrop->setLatitude(mapView->latitude());
	newDrop->setLongitude(mapView->longitude());
	//QString desc = QString("Coordinates: %1, %2").arg(mapView->latitude(),
	//		0, 'f', 3).arg(mapView->longitude(), 0, 'f', 3);
	newDrop->setName(title);
	newDrop->setDescription(subtitle);

	// use the marker in the assets, as opposed to the default marker
	Marker flag;
	flag.setIconUri(UIToolkitSupport::absolutePathFromUrl(
			QUrl("asset:///images/map/on_map_pin.png")));
	flag.setIconSize(QSize(60, 60));
	flag.setLocationCoordinate(QPoint(20, 59));
	flag.setCaptionTailCoordinate(QPoint(20, 1));
	newDrop->setMarker(flag);

	mapView->mapData()->add(newDrop);
}

void Dieta::updateMarkers(QObject* mapObject,
		QObject* containerObject) const
{
	MapView* mapview = qobject_cast<MapView*>(mapObject);
	Container* container = qobject_cast<Container*>(containerObject);

	for (int i = 0; i < container->count(); i++) {
		const QPoint xy = worldToPixel(mapview,
				container->at(i)->property("lat").value<double>(),
				container->at(i)->property("lon").value<double>());
		container->at(i)->setProperty("x", xy.x());
		container->at(i)->setProperty("y", xy.y());
	}
}


void Dieta::objednat(QObject* objednavkaItem)
{
	ObjednavkaItem *item = qobject_cast<ObjednavkaItem*>(objednavkaItem);
	if (item){
		qDebug() << "DIETA ID:" << item->dietaID() << endl << \
				"PRE ID:" << item->preID() << endl << \
				"Datum:" << item->datum() << endl << \
				"Dlzka programu:" << item->dlzkaProgramuID() << endl << \
				"Dorucenie ID:" << item->dorucenieID() << endl << \
				"Mesto ID:" << item->mestoID() << endl << \
				"Pobocka ID:" << item->pobockaID() << endl << \
				"Cas dorucenia ID:" << item->casDoruceniaID() << endl << endl << \
				"Meno:" << item->meno() << endl << \
				"Priezvisko:" << item->priezvisko() << endl << \
				"Telefon:" << item->telefon() << endl << \
				"Email:" << item->email() << endl << endl << \
				"Fa Meno:" << item->faMeno() << endl << \
				"Fa Ulica:" << item->faUlica() << endl << \
				"Fa Obec:" << item->faObec() << endl << \
				"Fa Psc:" << item->faPsc() << endl << \
				"Fa IC:" << item->faIc() << endl << \
				"Fa Dic:" << item->faDic() << endl << endl << \
				"Rovnaka adresa:" << item->rovnakaAdresa() << endl << \
				"Do Meno:" << item->doMeno() << endl << \
				"Do Ulica:" << item->doUlica() << endl << \
				"Do Obec:" << item->doObec() << endl << \
				"Do Psc:" << item->doPsc() << endl << \
				"Mam voucher:" << item->mamVoucher() << endl << \
				"Voucher:" << item->voucherKod() << endl << \
				"Poznamka:" << item->poznamka();

		api->objednat(item->dietaID(), item->preID(), item->datum(), item->dlzkaProgramuID(),
				item->dorucenieID(), item->mestoID(), item->pobockaID(), item->casDoruceniaID(),
				item->meno(), item->priezvisko(), item->telefon(), item->email(),
				item->faMeno(), item->faUlica(), item->faObec(), item->faPsc(), item->faIc(), item->faDic(),
				item->rovnakaAdresa(), item->doMeno(), item->doUlica(), item->doObec(), item->doPsc(),
				item->mamVoucher(), item->voucherKod(), item->poznamka());
	}
}


QPoint Dieta::worldToPixel(QObject* mapObject, double latitude,
		double longitude) const
{
	MapView* mapview = qobject_cast<MapView*>(mapObject);
	const Point worldCoordinates = Point(latitude, longitude);

	return mapview->worldToWindow(worldCoordinates);
}



void Dieta::saveToFile(const QString& filename, const QByteArray &data)
{
	QFile file(QString("data/%1").arg(filename));
	if (file.open(QIODevice::WriteOnly)){
		qDebug() << Q_FUNC_INFO << file.errorString() << endl;
	}else{
		file.write(data);
		file.flush();
		file.close();
	}
}

QByteArray Dieta::addTimestamp(const QByteArray& data, const int& hours)
{
	JsonDataAccess jda;
	QVariantMap map = jda.loadFromBuffer(data).toMap();
	map.insert("timestamp", Helper::instance()->getTimestamp(hours));
	QByteArray buffer;
	jda.saveToBuffer(map, &buffer);
	return buffer;
}


void Dieta::error(int errorCode, QString errorString)
{
	qDebug() << Q_FUNC_INFO << endl << "ERROR CODE:" << errorCode << endl << \
			"ERROR STRING:" << errorString;
	setLoading(false);
}


void Dieta::requestReady(const QByteArray response,
		const QNetworkReply* reply)
{
	Q_UNUSED(reply);
	const DietaLib::Request &request = api->request();
	switch (request){
	case DietaLib::PRIHLASENIE:
		parsePrihlasenie(response);
		break;
	case DietaLib::REGISTRACIA:
		parseRegistracia(response);
		break;
	case DietaLib::FACEBOOK_LOGIN:
		parsePrihlasenie(response);
		break;
	case DietaLib::MOJ_UCET:
		parseMojUcet(response);
		break;
	case DietaLib::OBJEDNAVKY:
		parseMojeObjednavky(response);
		break;
	case DietaLib::MOJA_OBJEDNAVKA:
		parseMojaObjednavka(response);
		break;
	case DietaLib::PRIDAT_OBJEDNAVKU:
		parsePridatObjednavku(response);
		break;
	case DietaLib::EDITOVAT_OBJEDNAVKU:
		parseEditovatObjednavku(response);
		break;
	case DietaLib::ULOZIT_OBJEDNAVKU:
		parseUlozitObjednavku(response);
		break;
	case DietaLib::ODSTRANIT_OBJEDNAVKU:
		parseOdstranitObjednavku(response);
		break;
	case DietaLib::REGION:
		parseRegion(response);
		break;
	case DietaLib::CAS_DORUCENIA:
		parseCasDorucenia(response);
		break;
	case DietaLib::DORUCENIE:
		parseDorucenie(response);
		break;
	case DietaLib::JEDALNICEK:
		parseJedalnicek(response);
		break;
	case DietaLib::OSOBNY_ODBER:
		parseOsobnyOdber(response);
		break;
	case DietaLib::OSOBNY_ODBER_MESTA:
		parseOsobnyOdberMesta(response);
		break;
	case DietaLib::DOPRAVA:
		parseDoprava(response);
		break;
	case DietaLib::ZMENIT_NASTAVENIE:
		parseZmenitNastavenie(response);
		break;
	case DietaLib::ADRESA_PRIDANIE:
		parsePridatAdresu(response);
		break;
	case DietaLib::ADRESA_EDITACIA:
		parseEditovatAdresu(response);
		break;
	case DietaLib::ADRESA_ODSTRANENIE:
		parseOdstranitAdresu(response);
		break;
	case DietaLib::OBJEDNAT:
		parseObjednat(response);
		break;
	default:
		break;
	}
}

void Dieta::parsePrihlasenie(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit prihlasenieDone(response);
}


void Dieta::parseRegistracia(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit registraciaDone(response);
}

void Dieta::parseMojUcet(const QByteArray response)
{
	QByteArray result = addTimestamp(response, 0);

	bool cached = Cacher::instance()->saveCache("moj_ucet", result);
	Q_ASSERT(cached);
	Q_UNUSED(cached);

	qDebug() << Q_FUNC_INFO << response;
	emit mojUcetDone(response);
}

void Dieta::parseMojeObjednavky(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit mojeObjednavkyDone(response);
}

void Dieta::parseMojaObjednavka(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit mojaObjednavkaDone(response);
}

void Dieta::parsePridatObjednavku(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit pridatObjednavkuDone(response);
}

void Dieta::parseEditovatObjednavku(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit editovatObjednavkuDone(response);
}

void Dieta::parseUlozitObjednavku(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit ulozitObjednavkuDone(response);
}

void Dieta::parseOdstranitObjednavku(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit odstranitObjednavkuDone(response);
}

void Dieta::parseRegion(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit regionDone(response);
}

void Dieta::parseCasDorucenia(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit casDoruceniaDone(response);
}

void Dieta::parseDorucenie(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit dorucenieDone(response);
}

void Dieta::parseJedalnicek(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit jedalnicekDone(response);
}

void Dieta::parseOsobnyOdber(const QByteArray response)
{
	QByteArray result = addTimestamp(response, 8);

	bool cached = Cacher::instance()->saveCache("osobny_odber", result);
	Q_ASSERT(cached);
	Q_UNUSED(cached);

	qDebug() << Q_FUNC_INFO << result;
	emit osobnyOdberDone(result);
}

void Dieta::parseOsobnyOdberMesta(const QByteArray response)
{
	QByteArray result = addTimestamp(response, 0);

	bool cached = Cacher::instance()->saveCache("osobny_odber_mesta", result);
	Q_ASSERT(cached);
	Q_UNUSED(cached);

	qDebug() << Q_FUNC_INFO << response;
	emit osobnyOdberMestaDone(response);
}

void Dieta::parseDoprava(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit dopravaDone(response);
}

void Dieta::parseZmenitNastavenie(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit zmenitNastavenieDone(response);
}

void Dieta::parsePridatAdresu(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit pridatAdresuDone(response);
}

void Dieta::parseEditovatAdresu(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit editovatAdresuDone(response);
}

void Dieta::parseOdstranitAdresu(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit odstranitAdresuDone(response);
}


void Dieta::parseObjednat(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit objednatDone(response);
}

void Dieta::parseFacebookLogin(const QByteArray response)
{
	qDebug() << Q_FUNC_INFO << response;
	emit facebookLoginDone(response);
}

void Dieta::finished(QNetworkReply* reply)
{
	if (!reply->error()){
		qDebug() << "Facebook done";
		JsonDataAccess jda;
		QVariantMap map = jda.loadFromBuffer(reply->readAll()).toMap();
		if (!map.isEmpty()){
			api->prihlasenieFacebook(_facebookMail, map.value("id").toString(),
					map.value("first_name").toString(), map.value("last_name").toString(), _accessTokenFacebook);
		}
	}else{
		qDebug() << "Facebook error";
		qDebug() << reply->errorString() << endl << \
				reply->error();
	}
	reply->deleteLater();
}
