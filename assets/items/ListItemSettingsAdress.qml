import bb.cascades 1.2

import "../components"

ListItemBase {
    id: root

    showNext: true
    
    Container {
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        verticalAlignment: VerticalAlignment.Center
        MyLabel {
            id: title
            fontFamily: "RobotoLight"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: ListItemData.title
        }
        
        MyLabel {
            id: subtitle
            topMargin: 0
            fontFamily: "RobotoLight"
            fontSize: FontSize.Small
            text: ListItemData.subtitle
        }
    
    }

}