import bb.cascades 1.2
import bb.cascades.maps 1.0
import bb.platform 1.2
import QtMobility.sensors 1.2
import QtMobilitySubset.location 1.1

import "../components"
import "../actions"

BasePage {
    id: root
    
    property variant location
    property alias lat: map.latitude
    property alias lon: map.longitude
    property alias title: titleHeader.headerText
    property string subtitle
    onLocationChanged: {
        map.latitude = location.lat
        map.longitude = location.lon
        console.log("LAT: ".concat(map.latitude).concat(" | LON: ".concat(map.longitude)))
        App.addPinToMap(map, title, subtitle)
    }
    header: TitleHeader {
        id: titleHeader
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onActiveChanged: {
        if (active){
            activePane.peekEnabled = false
        }else{
            activePane.peekEnabled = true
        }
    }
    
    actions: [
        NavigateAction {
            onClicked: {
                routeMapInvoker.go()
            }
        }
    ]
    
    attachedObjects: [
        Compass {
            property double azimuth: 0
            active: true
            axesOrientationMode: Compass.UserOrientation
            alwaysOn: false
            onReadingChanged: { // Called when a new compass reading is available
                map.setHeading(reading.azimuth);
                compassImage.rotationZ = 360 - reading.azimuth;
            }
        },
        RotationSensor {
            id: rotation
            property real x: 0
            active: true
            alwaysOn: false
            skipDuplicates: true
            onReadingChanged: {
                x = reading.x - 30
                if (x <= 40 && x > 0) {
                    map.setTilt(x);
                }
            }
        },
        PositionSource {
          	id: positionSource
          	active: true
          	updateInterval: 1000
        },
        RouteMapInvoker {
            id: routeMapInvoker
            
            startLatitude    :  positionSource.position.coordinate.latitude
            startLongitude   :  positionSource.position.coordinate.longitude
            startName: "Moje poloha"
            
            endLatitude      : map.latitude
            endLongitude     : map.longitude
            endName: title
            startDescription: subtitle
        }
    ]
    
    
    MapView {
        id: map
        objectName: "mapViewObject"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        altitude: 2000
        
        onCreationCompleted: {
            setRenderEngine("RenderEngine3D")
        }
        
        onCaptionButtonClicked: {
            routeMapInvoker.go() 
        }
    }
    Container {
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        topPadding: 20
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Bottom
        overlapTouchPolicy: OverlapTouchPolicy.Allow
        
        ImageView {
            id: compassImage
            imageSource: "asset:///images/map/compass.png"
            horizontalAlignment: HorizontalAlignment.Center
            attachedObjects: [
                ImplicitAnimationController {
                    // Disable animations to avoid jumps between 0 and 360 degree
                    enabled: false
                }
            ]
        }
    } // End of the Container for the pins
}