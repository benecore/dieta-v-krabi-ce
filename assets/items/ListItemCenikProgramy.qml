import bb.cascades 1.2

import "../components"

ListItemBase {
    id: root
    
    showNext: true
    preferredHeight: 120
    
    Container {
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        verticalAlignment: VerticalAlignment.Center
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        
        ImageView {
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            minHeight: 60
            maxHeight: 60
            scalingMethod: ScalingMethod.AspectFit
            verticalAlignment: VerticalAlignment.Center
            imageSource: ListItemData.icon
        }
        
        
        
        MyLabel {
            leftMargin: 40
            fontFamily: "RobotoLight"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: ListItemData.title
            textStyle.textAlign: TextAlign.Center
        }   
    }

}