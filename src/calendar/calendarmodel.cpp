/*
 * calendarmodel.cpp
 *
 *  Created on: 22.4.2014
 *      Author: benecore
 */

#include "calendarmodel.h"

CalendarModel::CalendarModel() :
_language(QLocale::Czech)
{
	this->setGrouping(ItemGrouping::None);
	this->setSortingKeys(QStringList() << "index");

	_localeHandler = new LocaleHandler;
	//bool res = QObject::connect(_localeHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
	// This is only available in Debug builds
	//Q_ASSERT(res);
	// Since the variable is not used in the app, this is added to avoid a
	// compiler warning
	//Q_UNUSED(res);

	generateModel();
}


CalendarModel::~CalendarModel() {
	clear();
	delete _localeHandler;
}


void CalendarModel::onSystemLanguageChanged()
{
	_language = QLocale().language();
	emit propertiesChanged();
}


bool CalendarModel::canEdit(const QDate& date)
{
	const QDateTime current = QDateTime(date);
	QDateTime today = QDateTime(QDate::currentDate());
	QDateTime today14 = QDateTime(today.addSecs(14*60*60));
	QDateTime today16 = QDateTime(today.addSecs(16*60*60));

	const int currentDayOfWeek = current.date().dayOfWeek();

	const QDateTime teraz = QDateTime::currentDateTime();
	qDebug() << "TERAZ:" << teraz.toString(Qt::ISODate);
	/*
	    qDebug() << "Today:" << today->toString(Qt::ISODate) << endl << \
	                "Today 14:" << today14->toString(Qt::ISODate) << endl << \
	                "Today 16:" << today16->toString(Qt::ISODate);
	 */

	if (currentDayOfWeek >= 1 || currentDayOfWeek <= 4){

		//qDebug() << "CURRENT BEFORE:" << today->toString("dd.MM.yyyy | hh:mm:ss");
		//today = new QDateTime(QDate::currentDate().addDays(2));
		today = today.addDays(2);
		//qDebug() << "CURRENT AFTER:" << today->toString("dd.MM.yyyy | hh:mm:ss");
	}else{
		qDebug() << "TU SOM";
		if (currentDayOfWeek == 5){ // Piatok
			if (teraz > today14){ // Piatok po 14:00 = editujem Stredu
				//today = new QDateTime(QDate::currentDate().addDays(5));
				today = today.addDays(5);
				//qDebug() << "STREDA:" << today->toString("dd.MM.yyyy | hh:mm:ss");
			}else{ // inak Pondelok
				//today = new QDateTime(QDate::currentDate().addDays(3));
				today = today.addDays(3);
				//qDebug() << "PONDELOK:" << today->toString("dd.MM.yyyy | hh:mm:ss");
			}
		}else{
			if (currentDayOfWeek == 6){ // sobota
				//today = new QDateTime(QDate::currentDate().addDays(4)); // editujem stredu
				today = today.addDays(4);
			}else{
				if (currentDayOfWeek == 7){ // nedela
					//today = new QDateTime(QDate::currentDate().addDays(5)); // editujem stredu
					today = today.addDays(3);
				}else{
					if (teraz > today16){
						//today = new QDateTime(QDate::currentDate().addDays(4));
						today = today.addDays(4);
						//qDebug() << today->toString("dd.MM.yyyy | hh:mm:ss");
					}else{
						//today = new QDateTime(QDate::currentDate().addDays(3));
						today = today.addDays(3);
					}
				}
			}
		}
	}

	// Ak je nedel pridame +24 hodin
	int dayOfWeek = today.date().dayOfWeek();
	if (dayOfWeek == 7){
		today = today.addDays(1);
	}

	qDebug() << "DNES:" << today.toString("dd.MM.yyyy | hh:mm:ss");

	bool edit;

	//qDebug() << "SECS TO:" << ;

	if (current > today && currentDayOfWeek != 7){
		edit = true;
	}else{
		edit = false;
	}

	return edit;
}


void CalendarModel::generateModel(int months)
{

	clear();
	scrollIndex = 0;
	QDate today = QDate::currentDate();
	currentDate = QDate::currentDate().addMonths(months);
	int daysInMonth = currentDate.daysInMonth();
	int firstDay = QDate(currentDate.year(), currentDate.month(), 1).dayOfWeek()-1;
	int start = 0;
	while (start != firstDay+daysInMonth){
		QVariantMap item;
		int result = start - firstDay + 1; // start - firstDay + 1
		if (start >= 0 && start <=6){
			item.insert("dayName", localizedDayName(start+1, QLocale::ShortFormat));
		}
		qDebug() << "CURRENT DATE" << currentDate << endl << "DAY DATE:" <<  QDate(currentDate.year(), currentDate.month(), result);
		if (today == QDate(currentDate.year(), currentDate.month(), result)){
			scrollIndex = start;
			item.insert("today", true);
		}else{
			item.insert("today", false);
		}
		item.insert("day", QDate(currentDate.year(), currentDate.month(), result).toString("dd"));
		item.insert("index", start);
		item.insert("dayLongName", localizedDayName(QDate(currentDate.year(), currentDate.month(), result).dayOfWeek()));
		item.insert("date", QDate(currentDate.year(), currentDate.month(), result).toString("dd.MM.yyyy"));
		item.insert("data", QVariantMap());
		item.insert("canEdit", canEdit(QDate(currentDate.year(), currentDate.month(), result)));
		insert(new CalendarItem(item, this));
		++start;
	}
	emit propertiesChanged();
	qDebug() << "GENERATED";
}


void CalendarModel::updateModel(const QVariantList& data)
{
	QList<QObject*>items = toListOfObjects();
	foreach(QObject *object, items){
		CalendarItem *item = qobject_cast<CalendarItem*>(object);
		foreach(const QVariant &remoteData, data){
			if (remoteData.toMap().value("datum").toString().trimmed() == item->date().toString().trimmed()){
				qDebug() << "Item:" << item->dayLongName();
				item->setData(remoteData.toMap());
			}
		}
	}
	qDebug() << "UPDATED";
}

int CalendarModel::todayIndex()
{
	return scrollIndex;
}



void CalendarModel::resetModel()
{
	clear();
	generateModel();
}


