#ifndef DIETA_GLOBAL_H
#define DIETA_GLOBAL_H

#include <QtCore/qglobal.h>

#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
#define QT5
#endif

#if defined(DIETA_LIBRARY)
#  define DIETASHARED_EXPORT Q_DECL_EXPORT
#else
#  define DIETASHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DIETA_GLOBAL_H
