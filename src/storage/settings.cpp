/*
 * settings.cpp
 *
 *  Created on: 24.2.2014
 *      Author: Benecore
 */

#include "settings.h"
#include "../custom/sizehelper.h"

Settings *Settings::_instance = 0;

Settings::Settings()
{
	settings = new QSettings("DevPDA Inc.", "FDB.cz");

	_saveLogin = settings->value("saveLogin", false).toBool();
}

Settings::~Settings()
{
	delete settings;
}


Settings *Settings::instance()
{
	if (!_instance)
		_instance = new Settings;
	return _instance;
}

void Settings::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}
