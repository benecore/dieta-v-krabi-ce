import bb.cascades 1.2

import "../components"
import "../actions"

BasePage {
    id: root
    
    signal objednat
    
    header: TitleHeader {
        headerText: "ŽIJU ZDRAVĚ ŽENA"
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    actions: [
        ObjednatAction {
            onClicked: {
                root.objednat()
            }
        }
    ]
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container{
            horizontalAlignment: HorizontalAlignment.Fill
            topPadding: 25
            leftPadding: 10
            rightPadding: 10
            
            ImageView {
                preferredWidth: 150
                scalingMethod: ScalingMethod.AspectFit
                horizontalAlignment: HorizontalAlignment.Center
                imageSource: "asset:///images/programy/zzz-icon.png"
            }
            
            MyLabel {
                fontSize: FontSize.Medium
                fontFamily: "RobotoRegular"
                multiline: true
                //text: "<html>Pokud patříte k té šťastnější části populace, která se při pohledu na váhu spokojeně usměje, je pro vás program ŽIJU ZDRAVĚ jako stvořený. Tento jídelníček se totiž nesnaží příjem potravy omezit, mnohem více dbá o pestrost a kvalitu stravy.<br/><br/>Program ŽIJU ZDRAVĚ plynule navazuje na program HUBNU. Již se nejedná o redukční jídelníček, ale o udržovací<br/><br/>Každý den <b>jídelníček zahrnuje:</b><br/>snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 8 000 kJ.</html>"
                text: Desc.zzdraveZenaText
            }
        } // end of root container          
    } // end of scroll view

}