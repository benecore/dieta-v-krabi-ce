import bb.cascades 1.2

import "../components"

ScrollView {
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        ImageView {
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: Paths.images+"/referencie/reference-ikony.png"
        }
        
        Divider{}
        
        Container {
            
            MyLabel {
                fontFamily: "RobotoBold"
                fontSize: FontSize.Medium
                text: "Michaela Štoudková, modelka"
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                ImageView {
                    preferredHeight: 300
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: Paths.images+"/referencie/reference-stoudkova.png"
                }
                
                MyLabel {
                    multiline: true
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Small
                    text: Desc.stoudkovaText
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
        } // end of Stoudkova
        
        Container {
            
            MyLabel {
                fontFamily: "RobotoBold"
                fontSize: FontSize.Medium
                text: "Patricie Solaříková, herečka"
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                ImageView {
                    preferredHeight: 300
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: Paths.images+"/referencie/reference-solarikova.png"
                }
                
                MyLabel {
                    multiline: true
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Small
                    text: Desc.solarikovaText
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
        } // end of solarikova
        
        Container {
            
            MyLabel {
                fontFamily: "RobotoBold"
                fontSize: FontSize.Medium
                text: "Marta Jandová, zpěvačka"
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                ImageView {
                    preferredHeight: 300
                    scalingMethod: ScalingMethod.AspectFit
                    imageSource: Paths.images+"/referencie/reference-jandova.png"
                }
                
                MyLabel {
                    multiline: true
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Small
                    text: Desc.jandovaText
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                }
            }
        }
    }

}