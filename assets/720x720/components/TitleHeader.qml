import bb.cascades 1.2



TitleBar {
    id: root
    
    property alias headerText: headerLabel.text
    property variant headerColor: Color.create("#6ebd43")
    property alias headerLeftImage: headerLeftImage.visible
    property alias headerLeftImageSource: headerLeftImage.imageSource
    property alias headerRightImage: headerRightImage.visible
    property alias headerRightImageSource: headerRightImage.imageSource
    property int headerLeftImageSourcePreferredHeight: 80
    property int headerRightImageSourcePreferredHeight: 80


    scrollBehavior: TitleBarScrollBehavior.Sticky
    kind: TitleBarKind.FreeForm
    kindProperties: FreeFormTitleBarKindProperties {
        Container {
            id: freeForm
            layout: DockLayout{}
            background: root.headerColor
            
            Container {
                leftPadding: 25
                rightPadding: 25
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                
                ImageView {
                    id: headerLeftImage
                    visible: true
                    preferredWidth: headerLeftImageSourcePreferredHeight
                    scalingMethod: ScalingMethod.AspectFit
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    imageSource: "asset:///images/header/hearth-home.png"
                }
                
                Container {
                    objectName: "headerLabelContainer"
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    verticalAlignment: VerticalAlignment.Center
                    leftPadding: !headerLeftImage.visible ? headerLeftImageSourcePreferredHeight : 0
                    rightPadding: !headerRightImage.visible ? headerRightImageSourcePreferredHeight : 0
                    MyLabel {
                        id: headerLabel
                        horizontalAlignment: HorizontalAlignment.Fill
                        textStyle.textAlign: TextAlign.Center
                        fontSize: FontSize.XLarge
                        textStyle.color: Color.White
                    }
                }// end of title container
                
                ImageView {
                    id: headerRightImage
                    visible: false
                    preferredWidth: headerRightImageSourcePreferredHeight
                    scalingMethod: ScalingMethod.AspectFit
                    verticalAlignment: VerticalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                }
            }
        } // end of header
    }

}