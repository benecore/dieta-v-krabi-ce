import bb.cascades 1.2

import "../components"

ListItemBase {
    id: root
    
    
    showNext: true
    
    
    Container {
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        verticalAlignment: VerticalAlignment.Center
        MyLabel {
            fontFamily: "RobotoLight"
            fontSize: FontSize.Large
            text: ListItemData.jmeno
            bottomMargin: 0
        }
        
        MyLabel {
            topMargin: 0
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: ListItemData.ulice.concat(" | ").concat(ListItemData.obec)
            bottomMargin: 0
        }
        MyLabel {
            topMargin: 0
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: ListItemData.psc
            bottomMargin: 0
        }
    
    }

}