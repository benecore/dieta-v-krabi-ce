import bb.cascades 1.2

import "../components"

ListItemBase {
    id: root

    showNext: false
    preferredHeight: 120

    
    Container {
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        verticalAlignment: VerticalAlignment.Center
        overlapTouchPolicy: OverlapTouchPolicy.Deny
        MyLabel {
            fontFamily: "RobotoLight"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: ListItemData.nazev
        }
        
        MyLabel {
            topMargin: 0
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Small
            text: ListItemData.adresa
            bottomMargin: 0
        }
        
        Label {
            visible: ListItemData.dopoledne
            topMargin: 0
            //fontFamily: "RobotoLight"
            textStyle.fontSize: FontSize.XXSmall
            text: ListItemData.dopoledne
            bottomMargin: 0
        }
        Label {
            visible: ListItemData.odpoledne
            topMargin: 0
            //fontFamily: "RobotoLight"
            textStyle.fontSize: FontSize.XXSmall
            text: ListItemData.odpoledne
        }
    }
    
    ImageView {
        verticalAlignment: VerticalAlignment.Center
        horizontalAlignment: HorizontalAlignment.Right
        id: nextIndicator
        visible: true
        imageSource: "asset:///images/expand-green_right.png"
    }
    
}