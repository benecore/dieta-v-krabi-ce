import bb.cascades 1.2

import "../components"
import "../actions"
import "../items"
import "../containers"
import "../pages"

TabPage {
    id: root
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    BasePage {
        id: page
        
        function osobnyOdberDone(response){
            model.clear()
            var json = JSON.parse(response)
            page.pobocek = json.pobocek
            model.append(json.data)
            indicator.stop()
        }
        
        property string pobocek
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        
        onCreationCompleted: {
            indicator.start()
            App.osobnyOdberDone.connect(osobnyOdberDone)
        }
        
        onActiveChanged: {
            if (active){
                var result = Cacher.getCache("osobny_odber")
                if (result == ""){
                    console.log("ONLINE")
                    Api.osobnyOdber()
                }else{
                    var json = JSON.parse(result)
                    if (json.timestamp <= Helper.getTimestamp()){
                        console.log("TIMESTAMP REACHED = ONLINE")
                        Api.osobnyOdber()
                    }else{
                        osobnyOdberDone(result)
                    }
                }
            }
        }
        
        actions: [
            RefreshAction {
                onClicked: {
                    indicator.start()
                    Api.osobnyOdber()
                }
            }
        ]
        
        attachedObjects: [
            ComponentDefinition {
                id: map
                Mapa {
                    
                }
            }
        ]
        
        
        ActivityIndicator {
            id: indicator
            preferredHeight: 200
            preferredWidth: 200
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }
        
        
        Container {
            visible: !indicator.running
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            opacity: App.loading ? 0 : 1
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoLight"
                fontSize: FontSize.Large
                text: page.pobocek
            }
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Medium
                text: "<html>Osobně si můžete Vaši krabičkovou dietu vyzvednout <b>ZDARMA</b> na adresách:</html>"
            }
            
            ListView {
                id: listView
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                dataModel: ArrayDataModel {
                    id: model
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ListItemOsobnyOdber {}
                    }
                ]
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    var item = dataModel.data(indexPath)
                    if (item){
                        var page = map.createObject()
                        page.title = item.nazev
                        page.subtitle = item.adresa
                        var loc = {};
                        loc.lat = item.lat
                        loc.lon = item.lon
                        page.location = loc
                        activePane.push(page)
                    }
                }
                scrollIndicatorMode: ScrollIndicatorMode.None
            } // end of listview
            
        } // end of rootcontainer
        
    
    } // end of BasePage
}