import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../actions"
import "../items"

BasePage {
    id: root
    
    function odstranitAdresu(response){
    
    }
    
    function refresh(response){
        model.clear()
        var json = JSON.parse(response)
        for (var item in json.data){
            model.append(json.data[item])
        }
    }
    
    property variant indexPath
    property variant adresy
    header: TitleHeader {
        headerText: "Nastavení adres".toUpperCase()
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        App.mojUcetDone.connect(refresh)
        App.odstranitAdresuDone.connect(odstranitAdresu)
        /*
         * void pridatAdresuDone(const QByteArray response);
         void editovatAdresuDone(const QByteArray response);
         void odstranitAdresuDone(const QByteArray response);
         */
    }
    
    onActiveChanged: {
        if (active){
            Api.loading = true
            var i = 0;
            for (var item in adresy){
                model.append(adresy[item])
            }
            Api.loading = false
        }
    }
    
    actions: [
        AddAction {
            onClicked: {
                var sheet = addAdres.createObject()
                sheet.open()
            }  
        },
        RefreshAction {
            onClicked: {
                Api.mojUcet()
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
            id: addAdres
            AddAdress {
                onDone: {
                    close()
                    listView.clearSelection()
                    toast.body = text
                    toast.show()
                    Api.mojUcet()
                }
                onCanceled: {
                    close()
                    listView.clearSelection()
                }
            }
        },
        SystemToast{
            id: toast
            autoUpdateEnabled: true
            button.label: ""
        }
    ]
    
    ActivityIndicator {
        preferredHeight: 200
        preferredWidth: 200
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        visible: Api.loading
        running: visible
    }
    
    ListView {
        id: listView
        opacity: Api.loading ? 0 : 1
        leftPadding: 20
        rightPadding: 20
        topPadding: 20
        bottomPadding: 20
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model
        }
        listItemComponents: [
            ListItemComponent {
                type: ""
                ListItemAdress {}
            }
        ]
        onTriggered: {
            clearSelection()
            select(indexPath)
            var item = dataModel.data(indexPath)
            if (item){
                var sheet = addAdres.createObject()
                sheet.adress = item
                sheet.edit = true
                sheet.open()
            }
        }
    } // end of listView

}