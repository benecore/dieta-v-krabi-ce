import bb.cascades 1.2

import "../components"
import "../actions"

BasePage {
    id: root
    
    signal objednat
    
    header: TitleHeader {
        headerText: "ŽIJU ZDRAVĚ MUŽ"
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    actions: [
        ObjednatAction {
            onClicked: {
                root.objednat()
            }
        }
    ]
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container{
            horizontalAlignment: HorizontalAlignment.Fill
            topPadding: 25
            leftPadding: 10
            rightPadding: 10
            
            ImageView {
                preferredWidth: 150
                scalingMethod: ScalingMethod.AspectFit
                horizontalAlignment: HorizontalAlignment.Center
                imageSource: "asset:///images/programy/zzm-icon.png"
            }
            
            MyLabel {
                fontSize: FontSize.Medium
                fontFamily: "RobotoRegular"
                multiline: true
                //text: "<html>Časy, kdy hubnout znamenalo obmotat ledničku řetězem a jíst pouze nevýrazná jídla bez chuti, jsou dávno pryč. Díky programu DIETAVKRABIČCE.CZ nemusíte zanevřít na své gurmetství a přitom posunete rafičku na váze o několik stupínku doleva.<br/><br/>Naše klientky dosahují po <b>měsíční kúře</b> průměrného váhového úbytku <b>5–7 kg</b>. U každé je samozřejmě úbytek individuální.<br/><br/>Každý den <b>jídelníček zahrnuje:</b><br/>snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 5 000 kJ.</html>"
                text: Desc.zzdraveMuzText
            }
        } // end of root container          
    } // end of scroll view

}