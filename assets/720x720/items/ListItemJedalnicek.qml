import bb.cascades 1.2

import "../components"

ListItemBase {
    id: root
    
    
    minHeight: 80
    showNext: true
    rotateNext: root.ListItem.selected ? 90 : 0
    
    
    Container {
        leftPadding: highlightFrameSize
        rightPadding: highlightFrameSize
        topPadding: highlightFrameSize
        bottomPadding: highlightFrameSize
        verticalAlignment: VerticalAlignment.Center
        MyLabel {
            fontFamily: "RobotoLight"
            fontSize: FontSize.Large
            text: ListItemData.den
        } 
    }

}