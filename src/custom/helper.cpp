#include "helper.h"
#include <time.h>

Helper *Helper::_instance = 0;


Helper::Helper()
{
}


Helper::~Helper()
{
}


Helper *Helper::instance()
{
	if (!_instance)
		_instance = new Helper;
	return _instance;
}

void Helper::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}

QString Helper::getMonthName(const int& month) const
{
	QString monthString = QLocale(QLocale::Czech).standaloneMonthName(QDate::currentDate().addMonths(month).month());
	monthString = monthString.left(1).toUpper()+monthString.mid(1);
	return monthString;
}

QString Helper::getDate(const int &day, const QVariant &format) {
	if (format.isNull()){
		return QDate::currentDate().addDays(day).toString(Qt::ISODate);
	}else{
		return QDate::currentDate().addDays(day).toString(format.toString());
	}
}

int Helper::getTimestamp(const int& hours)
{
	return QDateTime::currentDateTime().addSecs(hours*60*60).toTime_t();
}

QDateTime Helper::getDateObject(const int& startHour) const
{
	return QDateTime::currentDateTime().addSecs(startHour*60);
}
