import bb.cascades 1.2
import bb.system 1.2
import com.devpda.items 1.0

import "../components"
import "../actions"


BasePage {
    id: root
    
    
    function numericOnly (textin) {
        
        var m_strOut = new String (textin);
        m_strOut = m_strOut.replace(/[^\d.]/g,'');
        
        return m_strOut;
    } 
    
    
    function dorucenieCheck(){
        if (stejnaCheckbox.checked){
            return true;
        }else{
            if (dorucenieMenoFirma.text.length && dorucenieMesto.text.length && 
            dorucenieUlica.text.length && doruceniePSC.text.length){
                return true;
            }else{
                return false;
            }
        }
    }
    
    
    function voucherCheck(){
        if (poukazGroup.selectedValue == 0){
            return true;
        }else{
            if (poukazKod.text.length){
                return true;
            }else{
                return false;
            }
        }
    }
    
    
    function canContinue(){
        if (kontaktMeno.text.length && kontaktPriezvisko.text.length && kontaktTelefon.text.length && kontaktEmail.text.length &&
        fakturaciaMenoFirma.text.length && fakturaciaUlica.text.length && fakturaciaMesto.text.length && 
        fakturaciaPSC.text.length && dorucenieCheck() && voucherCheck()){
            return true
        }
        return false
    }
    
    
    function objednatDone(response){
        var json = JSON.parse(response)
        if (json.data && json.error == 0){
            toastOK.body = json.data
            toastOK.show()
        }else{
            toast.body = json.error
            toast.show()
        } 
    }
    
    
    property ObjednavkaItem objednavkaItem: null
    property bool allOK: false
    onObjednavkaItemChanged: {
        console.log("DATUM: "+objednavkaItem.datum)
    }
    header: TitleHeader {
        headerText: activeTab.title.toUpperCase()
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        App.objednatDone.connect(objednatDone)
    }
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: "Objednat"
            imageSource: activeTab.imageSource
            onTriggered: {
                if (canContinue()){
                    App.objednat(objednavkaItem)
                }else{
                	toast.body = "Prosím vyplňte všechny povinné údaje označené hvězdičkou"
                    toast.show()
                }
            }
        }
    ]
    
    attachedObjects: [
        SystemToast {
            id: toast
            autoUpdateEnabled: true
            button.label: "Zavřít"
            onFinished: {
                if (value == SystemUiResult.ButtonSelection){
                    cancel()
                }
            }
        },
        SystemToast {
            id: toastOK
            autoUpdateEnabled: true
            button.label: "Zavřít"
            onFinished: {
                if (value == SystemUiResult.ButtonSelection){
                    activeTab = homeTab
                }
            }
        }
    ]
    
    
    Container {
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        ImageView {
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: "asset:///images/objednavka3.png"
        }
        
        ScrollView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                
                Header {
                    title: "FAKTURACE"
                }
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "KONTAKTNÍ ÚDAJE"
                }
                
                Container {
                    id: kontaktneUdaje
                    
                    
                    TextField {
                        id: kontaktMeno
                        hintText: "Jméno*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.meno = text.trim()
                        }
                    }
                    TextField {
                        id: kontaktPriezvisko
                        hintText: "Příjmení*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.priezvisko = text.trim()
                        }
                    }
                    TextField {
                        id: kontaktTelefon
                        hintText: "Telefon*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.PhoneNumber
                        onTextChanging: {
                            objednavkaItem.telefon = text.trim()
                        }
                    }
                    TextField {
                        id: kontaktEmail
                        hintText: "E-mail*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.EmailAddress
                        onTextChanging: {
                            objednavkaItem.email = text.trim()
                        }
                    }
                } // end of kontaktne udaje
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "FAKTURAČNÍ ADRESA"
                }
                Container {
                    id: fakturacneUdaje
                    
                    TextField {
                        id: fakturaciaMenoFirma
                        hintText: "Jméno/Firma*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.faMeno = text.trim()
                        }
                    }
                    TextField {
                        id: fakturaciaUlica
                        hintText: "Ulice a č.p.*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.faUlica = text.trim()
                        }
                    }
                    TextField {
                        id: fakturaciaMesto
                        hintText: "Město*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.faObec = text.trim()
                        }
                    }
                    TextField {
                        id: fakturaciaPSC
                        hintText: "PSČ*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            fakturaciaPSC.text = root.numericOnly(text)
                            objednavkaItem.faPsc = fakturaciaPSC.text.trim()
                        }
                    }
                    TextField {
                        id: fakturaciaIC
                        hintText: "IČ"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.faIc = text.trim()
                        }
                    }
                    TextField {
                        id: fakturaciaDIC
                        hintText: "DIČ"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.faDic = text.trim()
                        }
                    }
                } // end of fakturacne udaje
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "DORUČOVACÍ ADRESA**"
                }
                
                MyLabel {
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.XSmall
                    text: "**pokud se liší od fakturační adresy"
                }
                
                CheckBoxRow {
                    id: stejnaCheckbox
                    text: "Stejná jako fakturační"
                    checked: false
                    onCheckedChanged: {
                        if (checked){
                            objednavkaItem.doMeno = ""
                            objednavkaItem.doUlica = ""
                            objednavkaItem.doObec = ""
                            objednavkaItem.doPsc = ""
                        }
                        objednavkaItem.rovnakaAdresa = checked ? "1" : "0"
                    }
                }
                
                Container {
                    id: dorucovaciaAdresa
                    visible: !stejnaCheckbox.checked
                    topMargin: 10
                    TextField {
                        id: dorucenieMenoFirma
                        hintText: "Jméno/Firma*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.doMeno = text.trim()
                        }
                    }
                    TextField {
                        id: dorucenieUlica
                        hintText: "Ulice a č.p.*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.doUlica = text.trim()
                        }
                    }
                    TextField {
                        id: dorucenieMesto
                        hintText: "Město*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            objednavkaItem.doObec = text.trim()
                        }
                    }
                    TextField {
                        id: doruceniePSC
                        hintText: "PSČ*"
                        textFormat: TextFormat.Auto
                        inputMode: TextFieldInputMode.Text
                        onTextChanging: {
                            doruceniePSC.text = root.numericOnly(text)
                            objednavkaItem.doPsc = doruceniePSC.text.trim()
                        }
                    }
                } // end of dorucenie adresa
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "POZNÁMKA"
                }
                
                TextField {
                    id: poznamka
                    hintText: "poznámka"
                    textFormat: TextFormat.Auto
                    inputMode: TextFieldInputMode.Text
                    onTextChanging: {
                        objednavkaItem.poznamka = text.trim()
                    }
                }
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "DÁRKOVÝ POUKAZ/VOUCHER"
                }
                
                TextField {
                    id: poukazKod
                    visible: (poukazGroup.selectedValue == 1)
                    hintText: "Kód voucheru*"
                    textFormat: TextFormat.Auto
                    inputMode: TextFieldInputMode.Text
                    onTextChanging: {
                        objednavkaItem.voucherKod = text.trim()
                    }
                } // end of voucher kod
                
                RadioGroup {
                    id: poukazGroup
                    selectedIndex: 0
                    options: [
                        Option {
                            text: "Nemám/nechci"
                            value: 0
                        },
                        Option {
                            text: "Uplatnit"
                            value: 1
                        }
                    ]
                    onSelectedOptionChanged: {
                        objednavkaItem.mamVoucher = selectedValue
                        if (selectedValue == 0){
                            objednavkaItem.voucherKod = ""
                        }
                    }
                }
            } // end of fakturacia
        } // end of Scroll view
    } // end of root container
}