import bb.cascades 1.2

import "../components"


ScrollView {
    id: root
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        MyLabel {
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            multiline: true
            text: "<html><b>CENÍK</b> rozvozu krabičkové diety.</html>"
        }
        
        MyLabel{
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            multiline: true
            text: "<html>Dietu v krabičce Vám doručíme na Vámi určené místo v regionech Praha, Praha-východ a Praha-západ.</html>"
        }
        Divider{}
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoBold"
            fontSize: FontSize.Small
            text: "zkušební týden"
            bottomMargin: 0
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "5 dnů"
                bottomMargin: 0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "495* Kč"
                bottomMargin: 0
            }
        } // end of zkusebni tyden
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoBold"
            fontSize: FontSize.Small
            text: "měsíc bez sobot"
            bottomMargin: 0
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "20 dnů"
                bottomMargin: 0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "1980* Kč"
                bottomMargin: 0
            }
        } // end of mesic bez sobot
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoBold"
            fontSize: FontSize.Small
            text: "měsíc se sobotami"
            bottomMargin: 0
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "24 dnů"
                bottomMargin: 0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "2376* Kč"
                bottomMargin: 0
            }
        } // end of mesic se sobotami
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoBold"
            fontSize: FontSize.Small
            text: "2 měsíce bez sobot"
            bottomMargin: 0
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "40 dnů"
                bottomMargin: 0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "3960* Kč"
                bottomMargin: 0
            }
        } // end of 2 mesice bez sobot
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoBold"
            fontSize: FontSize.Small
            text: "2 měsíce se sobotami"
            bottomMargin: 0
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "48 dnů"
                bottomMargin: 0
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            MyLabel {
                topMargin: 0
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Small
                text: "4752* Kč"
                bottomMargin: 0
            }
        } // end of 2 mesice se sobotami
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.XSmall
            text: "* Ceny rozvozu jsou uvedeny bez DPH 21%"
        }
        Divider{}
        
        MyLabel {
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            multiline: true
            text: "<html>Pokud si na jedno místo objednáte více jídel, cena už se zvyšuje pouze o 10Kč za jedno jídlo.</html>"
        }
    
    }
}