import bb.cascades 1.2

import "../actions"
import "../components"

BasePage {
    id: root
    
    property bool buttonsVisible: false
    property alias title: titleHeader.headerText
    header: TitleHeader {
        id: titleHeader
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
        headerRightImageSourcePreferredHeight: 50
    }
    backgroundColor: Color.create("#f8f8f8")
    
    actions: [
        ObjednatAction {
            onClicked: {
                activeTab = objednavkaTab
            }
        }
    ]
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.pinchToZoomEnabled: false
        Container {
            leftPadding: 20
            rightPadding: 20
            topPadding: 20
            bottomPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoRegular"
                fontSize: FontSize.Large
                text: "Žiju zdravě muž"
            }
            MyLabel {
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.Medium
                text: "<html>Celodenní strava obsahuje 5 chutných jídel. <b>Snídani, dopolední svačinu, oběd, odpolední svačinu</b> a <b>večeři</b> a to s celkovou energetickou hodnotou <b>10 000 kJ.</b></html>"
                bottomMargin: 0
            }
            
            Divider{}
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoBold"
                fontSize: FontSize.Small
                text: "2 měsíce bez sobot"
                bottomMargin: 0
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                //horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "40 dnů"
                        bottomMargin: 0
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "12760* Kč"
                        bottomMargin: 0
                    }
                }
                Button {
                    visible: buttonsVisible
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Right
                    text: "Objednat"
                    imageSource: "asset:///images/actions/objednat_black.png"
                }
            } // end of 2 mesice bez sobotami
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoBold"
                fontSize: FontSize.Small
                text: "2 měsíce se sobotami"
                bottomMargin: 0
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                //horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "48 dnů"
                        bottomMargin: 0
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "15312* Kč"
                        bottomMargin: 0
                    }
                }
                Button {
                    visible: buttonsVisible
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Right
                    text: "Objednat"
                    imageSource: "asset:///images/actions/objednat_black.png"
                }
            } // end of se sobotami
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoBold"
                fontSize: FontSize.Small
                text: "měsíc bez sobot"
                bottomMargin: 0
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                //horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "20 dnů"
                        bottomMargin: 0
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "6580* Kč"
                        bottomMargin: 0
                    }
                }
                Button {
                    visible: buttonsVisible
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Right
                    text: "Objednat"
                    imageSource: "asset:///images/actions/objednat_black.png"
                }
            } // mesic bez sobot
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoBold"
                fontSize: FontSize.Small
                text: "měsíc se sobotami"
                bottomMargin: 0
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                //horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "24 dnů"
                        bottomMargin: 0
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "7896* Kč"
                        bottomMargin: 0
                    }
                }
                Button {
                    visible: buttonsVisible
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Right
                    text: "Objednat"
                    imageSource: "asset:///images/actions/objednat_black.png"
                }
            } // mesic se sobotami
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoBold"
                fontSize: FontSize.Small
                text: "zkušební týden"
                bottomMargin: 0
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                //horizontalAlignment: HorizontalAlignment.Fill
                Container {
                    
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "5 dnů"
                        bottomMargin: 0
                    }
                    MyLabel {
                        topMargin: 0
                        multiline: true
                        fontFamily: "RobotoLight"
                        fontSize: FontSize.Small
                        text: "1745* Kč"
                        bottomMargin: 0
                    }
                }
                Button {
                    visible: buttonsVisible
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Right
                    text: "Objednat"
                    imageSource: "asset:///images/actions/objednat_black.png"
                }
            } // zkusebni tyden
            
            MyLabel {
                multiline: true
                fontFamily: "RobotoLight"
                fontSize: FontSize.XSmall
                text: "* Ceny jídel jsou uvedeny bez DPH 15%"
            }
        
        } // end of root container
    } // end of scroll view

}