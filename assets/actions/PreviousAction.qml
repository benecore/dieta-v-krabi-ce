import bb.cascades 1.2

ActionItem {
    id: root
    
    ActionBar.placement: ActionBarPlacement.OnBar
    
    property alias text: root.title
    property alias icon: root.imageSource
    signal clicked()
    
    
    title: "Předešlý měsíc"
    imageSource: "asset:///images/actions/previous.png"
    onTriggered: {
        root.clicked()
    }
}