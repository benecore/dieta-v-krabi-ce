import bb.cascades 1.2
import bb 1.0
import com.devpda.tools 1.0

import "../components"
import "../actions"
import "../items"

BasePage {
    id: root
    
    
    header: TitleHeader {
        headerText: aboutAction.title.toUpperCase()
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onActiveChanged: {
        if (active){
            aboutAction.enabled = false
        }else{
            aboutAction.enabled = true
        }
    }
    
    attachedObjects: [
        ApplicationInfo {
            id: info
        },
        Invoker {
            id: invoker
            onInvocationFailed: {
                console.log("Invokation failed")
            }
        }
    ]
    
    ScrollView {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.pinchToZoomEnabled: false
        
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                preferredHeight: 200
                scalingMethod: ScalingMethod.AspectFit
                imageSource: "asset:///images/ikona.png"
            }
            
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoRegular"
                fontSize: FontSize.Large
                text: "Dieta v Krabičce"
                bottomMargin: 0
            }
            
            MyLabel {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoLight"
                fontSize: FontSize.Medium
                text: "Verze: "+info.version
            }
            
            Header {
                title: "Vývojář"
            }
            
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoLight"
                fontSize: FontSize.Medium
                text: "Zoltán 'Benecore' Benke"
                bottomMargin: 0
            }
            MyLabel {
                topMargin: 5
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoBold"
                fontSize: FontSize.Medium
                text: "http://devpda.net"
                textStyle.color: Color.create("#6ebd43")
                onTouch: {
                    invoker.target = "sys.browser"
                    invoker.action = "bb.action.OPEN"
                    invoker.uri = text
                    invoker.invoke()
                }
            }
            MyLabel {
                multiline: true
                horizontalAlignment: HorizontalAlignment.Center
                fontFamily: "RobotoLight"
                fontSize: FontSize.Medium
                text: "V případě problémů s aplikací mě neváhejte kontaktovat"
                bottomMargin: 0
            }
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                maxHeight: 70
                maxWidth: 70
                imageSource: "asset:///images/actions/mail.png"
                onClicked: {
                    invoker.target = "sys.pim.uib.email.hybridcomposer"
                    invoker.action = "bb.action.SENDEMAIL"
                    invoker.uri = "mailto:support@devpda.net?subject=Dieta (BB10)"
                    invoker.invoke()
                }
            }            
        } // end of root container
    } // end of scrollview
}