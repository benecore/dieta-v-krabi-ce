import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../actions"
import "../containers"

TabPage {
    id: root
    
    BasePage {
        id: page
        
        function request(response){
            var json = JSON.parse(response)
            toast.body = json.data
            toast.show()
        }
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        onCreationCompleted: {
            App.dopravaDone.connect(request)
            controlDelegate.sourceComponent = info
        }
        
        attachedObjects: [
            ComponentDefinition {
              	id: info
              	DopravaInfo {
                   
               }
            },
            ComponentDefinition {
                id: rozvoz
                DopravaRozvoz {
                    onZjistit: {
                        casDovozu.show()
                    }
                }
            },
            ComponentDefinition {
              	id: cena
              	DopravaCena {
                   
               }  
            },
            SystemToast {
                id: toast
                button.label: ""
                autoUpdateEnabled: true
            },
            SystemPrompt {
                id: casDovozu
                title: "Zjistit čas dovozu"
                rememberMeChecked: false
                includeRememberMe: false
                inputField.emptyText: "psč"
                onFinished: {
                    if (value == SystemUiResult.ConfirmButtonSelection){
                        console.debug("Input text".concat(inputFieldTextEntry()))
                        Api.doprava(inputFieldTextEntry().trim())
                    }
                }
                confirmButton.label: "Zjistit"
                returnKeyAction: SystemUiReturnKeyAction.Submit
            }
        ]
        
        
        ActivityIndicator {
            id: indicator
            preferredHeight: 200
            preferredWidth: 200
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Center
        }
        
        
        Container {
            visible: !indicator.running
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            SegmentedControl {
                id: vyberTyzdna
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: "Doprava"
                        value: info
                    },
                    Option {
                        text: "Časy rozvozu"
                        value: rozvoz
                    },
                    Option {
                        text: "Cena rozvozu"
                        value: cena
                    }
                ]
                onSelectedOptionChanged: {
                    controlDelegate.sourceComponent = selectedValue
                }
            }
            
            ControlDelegate {
                id: controlDelegate
                delegateActive: true
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
            
        } // end of root container
    
    }
}