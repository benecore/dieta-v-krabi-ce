import bb.cascades 1.2

import "../components"
import "../actions"
import "../items"
import "../containers"

TabPage {
    id: root
    
    BasePage {
        id: page
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
            headerRightImageSourcePreferredHeight: 50
        }
        backgroundColor: Color.create("#f8f8f8")
        
        
        onActiveChanged: {
            if (active){
                controlDelegate.sourceComponent = programyDelegate
            }
        }
        
        actions: [
            OsobnyOdberAction {
                onClicked: {
                    activeTab = osobnyOdberTab
                }
            }
        ]
        
        
        attachedObjects: [
            ComponentDefinition {
                id: programyDelegate
                CenikProgramy {
                
                }           
            },
            ComponentDefinition {
                id: rozvozDelegate
                CenikRozvoz {
                    
                }
            }
        ]
        
        
        Container {
            topPadding: 10
            leftPadding: 10
            rightPadding: 10
            bottomPadding: 10
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            SegmentedControl {
                id: segControl
                horizontalAlignment: HorizontalAlignment.Fill
                options: [
                    Option {
                        text: "Programy"
                        value: programyDelegate
                    },
                    Option {
                        text: "Rozvoz"
                        value: rozvozDelegate
                    }
                ]
                onSelectedOptionChanged: {
                    controlDelegate.sourceComponent = selectedValue
                }
            }
            ControlDelegate {
                id: controlDelegate
                delegateActive: true
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
        
        }
    
    
    }
}