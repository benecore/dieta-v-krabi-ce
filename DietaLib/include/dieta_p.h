#ifndef DIETALIB_P_H
#define DIETALIB_P_H

#include "dieta.h"
#include <QCryptographicHash>

class QNetworkAccessManager;
class QNetworkRequest;
class QUrl;
class QEventLoop;
class DIETASHARED_EXPORT DietaLibPrivate
{
public:
    DietaLibPrivate(DietaLib *parent = 0);
    virtual ~DietaLibPrivate();


    void executeRequest(const QUrl &requestUrl);
    void setRequest(const DietaLib::Request value);
    QByteArray getSignatureHash(const QByteArray &user = "bb");
    QByteArray getLoginHash(const QByteArray &mail, const QByteArray &password);
    QByteArray generateDateHeader();



private:
    QNetworkAccessManager *conn;
    QEventLoop *loop;
    DietaLib::Request request;
    DietaLib::DietaError error;
    QByteArray timestamp;
    QList<QString> days;
    QList<QString> months;
    bool synchronous;
    QString errorString;
    int errorCode;
    bool debug;
    QString _userMail;
    QString _userPassword;
    bool _loading;


protected:
    DietaLib *const q_ptr;
    Q_DECLARE_PUBLIC(DietaLib)
};


#endif // DIETALIB_P_H
