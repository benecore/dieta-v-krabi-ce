import bb.cascades 1.2

import "../components"
import "../items"
import "../sheets"

TabPage {
    id: root
    
    
    BasePage {
        id: page
        
        function loadImages(){
            var i = 100;
            for (var i = 100; i != 117; ++i){
                model.append(Paths.images+"/fotogalerie/".concat(i).concat(".png"));
            }
        }
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        onActiveChanged: {
            if (active){
                loadImages()
            }
        }
        
        
        attachedObjects: [
            ComponentDefinition {
                id: galeria
                Galeria {
                    onClosed: {
                        listView.clearSelection()
                    }
                }
            }
        ]
        
        ListView {
            id: listView
            leftPadding: 10
            rightPadding: 10
            topPadding: 10
            bottomPadding: 10
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill
            dataModel: ArrayDataModel {
                id: model
            }
            layout: GridListLayout {
                columnCount: 4
                horizontalCellSpacing: 10.0
                verticalCellSpacing: 10.0
            
            }
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    ListItemFotogaleria{}
                }
            ]
            onTriggered: {
                clearSelection()
                select(indexPath)
                var sheet = galeria.createObject()
                sheet.model = model
                sheet.indexPath = indexPath
                sheet.open()
            }
        } // end of listView
    
    } // end of Page
}