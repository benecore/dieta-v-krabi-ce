import bb.cascades 1.2

import "../components"

ListView {
    onCreationCompleted: {
        dataModel.append(Paths.images+"/fotogalerie/spj1.png")
        dataModel.append(Paths.images+"/fotogalerie/spj2.png")
        dataModel.append(Paths.images+"/fotogalerie/spj3.png")
        dataModel.append(Paths.images+"/fotogalerie/spj4.png")
        dataModel.append(Paths.images+"/fotogalerie/spj5.png")
        dataModel.append(Paths.images+"/fotogalerie/spj6.png")
        dataModel.append(Paths.images+"/fotogalerie/spj7.png")
        dataModel.append(Paths.images+"/fotogalerie/spj8.png")
    }
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    dataModel: ArrayDataModel {
    }
    listItemComponents: [
        ListItemComponent {
            type: ""
            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                preferredWidth: Qt.Size.maxWidth
                scalingMethod: ScalingMethod.AspectFit
                imageSource: ListItemData
            }
        }
    ]
    bufferedScrollingEnabled: true
    scrollIndicatorMode: ScrollIndicatorMode.None
}