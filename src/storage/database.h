/*
 * database.h
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <QObject>
#include <QtSql>
#include <QList>
#include <QDebug>
#include <bb/cascades/CustomControl>

using namespace bb::cascades;

class Database : public bb::cascades::CustomControl
{
	Q_OBJECT
	Q_PROPERTY(bool isLogged READ isLogged NOTIFY databaseChanged)
public:
	Database();
	virtual ~Database();

	static Database *instance();
	static void destroy();

	Q_INVOKABLE
	bool setLogin(const QString &email, const QString &password);
	Q_INVOKABLE
	QVariantMap getLogin();
	Q_INVOKABLE
	bool clearLogin();


private slots:
	inline bool isLogged() { return !getLogin().value("email").isNull(); }

signals:
	void databaseChanged();


protected:
	void createTables();


private:
	Q_DISABLE_COPY(Database)
	static Database *_instance;
	QSqlDatabase *db;
};

#endif /* DATABASE_H_ */
