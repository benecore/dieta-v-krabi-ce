import bb.cascades 1.2

Container {
    id: root
    
    
    property alias text: label.text
    property alias checked: checkBox.checked
    
    signal checkedChanged(bool checked)
    
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    
    
    CheckBox {
        id: checkBox
        verticalAlignment: VerticalAlignment.Center
        onCheckedChanged: {
            root.checkedChanged(checked)
        }
    }
    
    MyLabel {
        id: label
        fontSize: FontSize.Medium
    }
    
}