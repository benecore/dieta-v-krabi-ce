import bb.cascades 1.2

import "../components"

TabPage {
    id: root
    
    
    BasePage {
        
        header: TitleHeader {
            headerText: "DIETA V KRABIČCE"
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundImage: "asset:///images/backgrounds/home-background.png"
        
        ScrollView {
            scrollRole: ScrollRole.Main
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill
            Container {
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 100
                    ImageButton {
                        rightMargin: 80
                        defaultImageSource: "asset:///images/menu/nase-programy.png"
                        pressedImageSource: "asset:///images/menu/nase-programy_pressed.png"
                        onClicked: {
                            activeTab = naseProgramyTab
                        }
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///images/menu/jedalnicek.png"
                        pressedImageSource: "asset:///images/menu/jedalnicek_pressed.png"
                        onClicked: {
                            activeTab = jedalnicekTab
                        }
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 100
                    ImageButton {
                        rightMargin: 80
                        defaultImageSource: "asset:///images/menu/cennik.png"
                        pressedImageSource: "asset:///images/menu/cennik_pressed.png"
                        onClicked: {
                            activeTab = cenikTab
                        }
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///images/menu/objednavka.png"
                        pressedImageSource: "asset:///images/menu/objednavka_pressed.png"
                        onClicked: {
                            activeTab = objednavkaTab
                        }
                    }
                }
                
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 100
                    ImageButton {
                        rightMargin: 80
                        defaultImageSource: "asset:///images/menu/osobny-odber.png"
                        pressedImageSource: "asset:///images/menu/osobny-odber_pressed.png"
                        onClicked: {
                            activeTab = osobnyOdberTab
                        }
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///images/menu/doprava.png"
                        pressedImageSource: "asset:///images/menu/doprava_pressed.png"
                        onClicked: {
                            activeTab = dopravaTab
                        }
                    }
                }
            } 
        }
    }
}