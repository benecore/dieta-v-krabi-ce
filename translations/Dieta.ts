<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>Descriptions</name>
    <message utf8="true">
        <source>&lt;html&gt;Časy, kdy hubnout znamenalo obmotat ledničku řetězem a jíst pouze nevýrazná jídla bez chuti, jsou dávno pryč. Díky programu DIETAVKRABIČCE.CZ nemusíte zanevřít na své gurmetství a přitom posunete rafičku na váze o několik stupínku doleva.&lt;br/&gt;&lt;br/&gt;Naše klientky dosahují po &lt;b&gt;měsíční kúře&lt;/b&gt; průměrného váhového úbytku &lt;b&gt;5–7 kg&lt;/b&gt;. U každé je samozřejmě úbytek individuální.&lt;br/&gt;&lt;br/&gt;Každý den &lt;b&gt;jídelníček zahrnuje:&lt;/b&gt;&lt;br/&gt;snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 5 000 kJ.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>&lt;html&gt;Časy, kdy hubnout znamenalo obmotat ledničku řetězem a jíst pouze nevýrazná jídla bez chuti, jsou dávno pryč. Díky programu DIETAVKRABIČCE.CZ nemusíte zanevřít na své gurmetství a přitom posunete rafičku na váze o několik stupínku doleva.&lt;br/&gt;&lt;br/&gt;Naši klienti dosahují po &lt;b&gt;měsíční kúře&lt;/b&gt; průměrného váhového úbytku &lt;b&gt;5–7 kg&lt;/b&gt;. U každé je samozřejmě úbytek individuální.&lt;br/&gt;&lt;br/&gt;Každý den &lt;b&gt;jídelníček zahrnuje:&lt;/b&gt;&lt;br/&gt;snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 7 000 kJ.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>&lt;html&gt;Pokud patříte k té šťastnější části populace, která se při pohledu na váhu spokojeně usměje, je pro vás program ŽIJU ZDRAVĚ jako stvořený.Tento jídelníček se totiž nesnaží příjem potravy omezit, mnohem více dbá o pestrost a kvalitu stravy.&lt;br/&gt;&lt;br/&gt;Program ŽIJU ZDRAVĚ plynule navazuje na program HUBNU. Již se nejedná o redukční jídelníček, ale o udržovací.&lt;br/&gt;&lt;br/&gt;Každý den &lt;b&gt;jídelníček zahrnuje:&lt;/b&gt;&lt;br/&gt;snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 8 000 kJ.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>&lt;html&gt;Pokud patříte k té šťastnější části populace, která se při pohledu na váhu spokojeně usměje, je pro vás program ŽIJU ZDRAVĚ jako stvořený. Tento jídelníček se totiž nesnaží příjem potravy omezit, mnohem více dbá o pestrost a kvalitu stravy.&lt;br/&gt;&lt;br/&gt;Program ŽIJU ZDRAVĚ plynule navazuje na program HUBNU. Již se nejedná o redukční jídelníček, ale o udržovací.&lt;br/&gt;&lt;br/&gt;Každý den &lt;b&gt;jídelníček zahrnuje:&lt;/b&gt;&lt;br/&gt;snídani, dopolední svačinu, oběd, odpolední svačinu a večeři; o celkové energetické hodnotě 10 000 kJ.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>&lt;html&gt;Naše krabičková dieta je připravována výhradně &lt;b&gt;z čerstvých a kvalitních surovin pod přísným hygienickým dohledem.&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;Všechna jídla jsou vyvíjená &lt;b&gt;profesionálním týmem kuchařů&lt;/b&gt; s bohatými zkušenostmi s domácí i mezinárodní kuchyní.&lt;br/&gt;&lt;br/&gt;Pro přípravu krabičkové diety využíváme &lt;b&gt;nejmodernějších přístrojů&lt;/b&gt;. Veškeré vybavení naší kuchyně splňuje ty nejnáročnější požadavky moderního a zdravého zařízení.&lt;br/&gt;&lt;br/&gt;Naše výrobna je &lt;b&gt;držitelem certifikátu HACCP&lt;/b&gt;. Tento certifikát zaručuje bezpečnost a především vysokou kvalitu našich programů ve všech fázích manipulace s pokrmy, surovinami, skladováním a distribucí.&lt;br/&gt;&lt;br/&gt;Jako jediná společnost na trhu krabičkových diet se v současné době certifikujeme na &lt;b&gt;ISO normu 22000&lt;/b&gt;, což je nejvyšší možná mezinárodní norma, která specifikuje požadavky na systém bezpečnosti potravin pro všechny organizace v potravinovém řetězci, které musí být naplněny, aby byla zajištěna bezpečnost potravin.&lt;br/&gt;&lt;br/&gt;&lt;b&gt;Adresa výrobny:&lt;/b&gt;&lt;br/&gt;K letišti 1018/55&lt;br/&gt;Praha 6, 16008&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>&lt;html&gt;Dietu v krabičce Vám doručíme na Vámi určené místo v regionech Praha, Praha-východ a Praha-západ nebo si jí můžete &lt;b&gt;ZDARMA&lt;/b&gt; osobně vyzvednout na na našich odběrných místech. Seznam našich odběrných míst naleznete v sekci &lt;b&gt;OSOBNÍ ODBĚR&lt;/b&gt;.&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Zpěvačka Marta Jandová vyzkoušela krabičkovou dietu i se svým přítelem. ,, Jelikož mám velice nabitý pracovní program a jsem stále na cestách, krabičková dieta DIETAVKRABICCE.CZ je pro mě ideální. Je to velice pohodlné a zároveň zdravé stravování. Jelikož nemám moc času na vaření, krabičkovou dietu jsem objednala i pro svého přítele. Jídlo je moc chutné, dokonce i kolegové z muzikálu mi ho záviděli.</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Patricie se pro DIETAVKRABICCE.CZ rozhodla po loňském pobytu ve Spojených státech Amerických, kde nabrala pár kil, se kterými se rozhodla zatočit. ,,Popustila jsem uzdu a jednoho dne jsem si řekla : Už dost!“ ,,Jelikož mi hodně času zabírá studium vysoké školy a natáčení seriálu Ulice a nemám tedy dostatek času na vaření, rozhodla jsem se do toho jít s DIETAVKRABICCE.CZ.“ ,,Za měsíc jsem shodila 3,5kg a tím jsem se dostala na ideální váhu, větší úbytek váhy po jednom měsíci by pro mě nebyl ani zdravý a hrozilo by mi riziko JOJO efektu, takto můj úspěch zhodnotil výživový poradce Mgr. Tomáš Lavický.“ ,,Krabičková dieta opravdu nepatří mezi ty drastické diety, měla jsem pocit, že celý den jím, sice po menších porcích, ale za to často,“ směje se Patricie.</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Míša se DIETAVKRABICCE.CZ rozhodla vyzkoušet v období, kdy se musela učit na státnice na vysoké škole. ,,Měla jsem čas pouze na učení, proto jsem nebyla schopná si sama vařit. Každý moc dobře ví, že pro práci modelky je štíhlá postava velice důležitá, vsadila jsem tedy na krabičkovou dietu a musím říct, že jsem byla moc spokojená, je to velice pohodlný a zdravý způsob stravování, nejraději bych tohoto ,,full servisu“ využívala pořád“, dodává Míša.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MyAccount</name>
    <message>
        <source>Tag it</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
