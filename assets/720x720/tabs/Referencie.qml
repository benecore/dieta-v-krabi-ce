import bb.cascades 1.2

import "../components"
import "../containers"

TabPage {
    id: root
    
    
    BasePage {
        
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        
        onActiveChanged: {
            if (active){
                controlDelegate.sourceComponent = klienti
            }
        }
        
        
        attachedObjects: [
            ComponentDefinition {
                id: klienti
                ReferencieKlienti{
                    
                }
            },
            ComponentDefinition {
                id: osobnosti
                ReferencieOsobnosti{
                    
                }
            }
        ]
        
        
        Container {
            topPadding: 20
            leftPadding: 20
            rightPadding: 20
            bottomPadding: 20
            SegmentedControl {
                horizontalAlignment: HorizontalAlignment.Fill
                selectedIndex: 0
                options: [
                    Option {
                        text: "Naši klienti"
                        value: klienti
                    },
                    Option {
                        text: "Známé osobnosti"
                        value: osobnosti
                    }
                ]
                onSelectedOptionChanged: {
                    controlDelegate.sourceComponent = selectedValue
                }
            }
            
            ControlDelegate {
                id: controlDelegate
                delegateActive: true
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1
                }
            }
        
        }
    }
}