import bb.cascades 1.2
import bb.cascades.pickers 1.0
import com.devpda.items 1.0

import "../components"
import "../actions"


BasePage {
    id: root
    
    function request(response){
        var json = JSON.parse(response)
        for (var item in json.data){
            var obj = option.createObject()
            obj.text = json.data[item].nazev
            obj.value = json.data[item].id
            mestaDropdown.add(obj)
        }
        App.loading = false
    }
    
    function request2(response){
        pobockaDropdown.removeAll()
        var json = JSON.parse(response)
        for (var item in json.data){
            if (mestaDropdown.selectedValue == 1){
                if (json.data[item].mesta_id == 1){
                    var obj = option.createObject()
                    obj.text = json.data[item].nazev + " | " + json.data[item].adresa
                    obj.value = json.data[item].id
                    pobockaDropdown.add(obj)
                }
            }else{
                if (json.data[item].mesta_id == 2){
                    var obj = option.createObject()
                    obj.text = json.data[item].nazev + " | " + json.data[item].adresa
                    obj.value = json.data[item].id
                    pobockaDropdown.add(obj)
                }
            }
        }
        App.loading = false
    }
    
    
    function canContinue(){
        if (mestaDropdown.selectedOption && pobockaDropdown.selectedOption || casyGroup.selectedOption){
            allOK = true
        }else{
            allOK = false
        }
    } 
    
    property ObjednavkaItem objednavkaItem: null
    onObjednavkaItemChanged: {
        console.log("DATUM: "+objednavkaItem.datum)
    }
    property bool allOK: false
    header: TitleHeader {
        headerText: activeTab.title.toUpperCase()
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    
    actions: [
        ActionItem {
            enabled: root.allOK
            ActionBar.placement: ActionBarPlacement.OnBar
            title: "FAKTURACE"
            imageSource: "asset:///images/actions/next.png"
            onTriggered: {
                var page = objednavka3.createObject()
                page.objednavkaItem = objednavkaItem
                activePane.push(page)
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
            id: option
            Option {
                
            }
        },
        ComponentDefinition {
            id: objednavka3
            Objednavka3 {
                
            }
        }
    ]
    
    onActiveChanged: {
        mestaDropdown.selectedOptionChanged.connect(canContinue)
        pobockaDropdown.selectedOptionChanged.connect(canContinue)
        casyGroup.selectedOptionChanged.connect(canContinue)
        
        App.osobnyOdberMestaDone.connect(request)
        App.osobnyOdberDone.connect(request2)
        if (active){
            App.loading = true
            var data = Cacher.getCache("osobny_odber_mesta")
            if (data != ""){
                console.log("OFFLINE")
                var json = JSON.parse(data)
                if (json.timestamp <= Helper.getTimestamp()){
                    console.log("TIMESTAMP REACHED = ONLINE")
                    Api.osobnyOdberMesta()
                }else{
                    request(data)
                }
            }else{
                console.log("ONLINE")
                Api.osobnyOdberMesta()
            }
        }
    }
    
    
    ActivityIndicator {
        id: indicator
        preferredHeight: 300
        preferredWidth: 300
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        visible: App.loading
        running: App.loading
    }
    
    Container {
        topPadding: 10
        leftPadding: 10
        rightPadding: 10
        bottomPadding: 10
        ImageView {
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: "asset:///images/objednavka2.png"
        }
        
        ScrollView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                
                Header {
                    title: "DORUČENÍ"
                }
                
                MyLabel {
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "ZPŮSOB DORUČENÍ*"
                }
                
                RadioGroup {
                    id: dorucenieGroup
                    options: [
                        Option {
                            text: "Osobní odběr"
                            value: 1
                        },
                        Option {
                            text: "Dovoz"
                            value: 2
                        }
                    ]
                    onSelectedValueChanged: {
                        objednavkaItem.dorucenieID = selectedValue
                        if (selectedValue == 1){
                            casyGroup.selectedOption = null
                            objednavkaItem.casDoruceniaID = 0
                        }else{
                            objednavkaItem.mestoID = 0
                            objednavkaItem.pobockaID = 0
                            mestaDropdown.selectedOption = null
                            pobockaDropdown.selectedOption = null
                        }
                    }
                } // end of dorucenie
                
                MyLabel {
                    visible: (dorucenieGroup.selectedValue == 2)
                    fontFamily: "RobotoBold"
                    fontSize: FontSize.Medium
                    text: "ČAS DORUČENÍ*"
                }
                
                RadioGroup {
                    id: casyGroup
                    visible: (dorucenieGroup.selectedValue == 2)
                    options: [
                        Option {
                            text: "15:00 - 17:00"
                            value: 4
                        },
                        Option {
                            text: "16:00 - 18:00"
                            value: 5
                        },
                        Option {
                            text: "20:00 - 22:00"
                            value: 6
                        }
                    ]
                    onSelectedOptionChanged: {
                        objednavkaItem.casDoruceniaID = selectedValue
                    }
                } // end of casy dorucenia
                
                
                DropDown {
                    id: mestaDropdown
                    visible: (dorucenieGroup.selectedValue == 1)
                    title: "Místo odběru*"
                    onSelectedOptionChanged: {
                        objednavkaItem.mestoID = selectedValue
                        var odberMiesta = Cacher.getCache("osobny_odber")
                        if (odberMiesta != ""){
                            console.log("OFFLINE")
                            var json = JSON.parse(odberMiesta)
                            if (json.timestamp <= Helper.getTimestamp()){
                                console.log("TIMESTAMP REACHED = ONLINE")
                                Api.osobnyOdber()
                            }else{
                                request2(odberMiesta)
                            }
                        }else{
                            console.log("ONLINE")
                            Api.osobnyOdber()
                        }
                    }
                } // end of mesta dropdown
                
                DropDown {
                    id: pobockaDropdown
                    enabled: (mestaDropdown.selectedOption != 0)
                    visible: (dorucenieGroup.selectedValue == 1)
                    title: "Pobočka*"
                    onSelectedOptionChanged: {
                        objednavkaItem.pobockaID = selectedValue
                    }
                } // end of picker
            }
        }
    }
}