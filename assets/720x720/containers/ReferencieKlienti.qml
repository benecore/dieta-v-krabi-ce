import bb.cascades 1.2

import "../components"

ScrollView {
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        
        ImageView {
            scalingMethod: ScalingMethod.AspectFit
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: Paths.images+"/referencie/reference-kolace.png"
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "Zde pro Vás vybíráme z e-mailů naší zákaznické linky výběr reakcí našich klientů, za které velice děkujeme."
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "<html><b>LENKA P. - 21.03.2014</b>\nDobrý den, jídlo mi chutná a jsem spokojena, že se objevuje i zahraniční kuchyně.</html>"
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "<html><b>LUCIE V. - 19.03.2014</b>\nJídlo je velice chutné a neošizené. Jím stejné věci jako před tím, ale v menších porcích, ale hlavně opravdu pravidelně. Porce jsou malé, ale jídla je ve výsledku možná i více, než se mi někdy podařilo sníst za celý den.</html>"
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "<html><b>LENKA P. - 21.03.2014</b>\nDobrý den, jídlo mi chutná a jsem spokojena, že se objevuje i zahraniční kuchyně.</html>"
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "<html><b>LYDIE K. - 09.03.2014</b>\nDobrý den, krabičky si odebírám už 2. měsíc, původně jsem chtěla zkusit jeden měsíc, ale jsem stále nadšená, je mi dobře, vše je velmi chutné, sladěné, zajímavé, sama bych to nedokázala. Řadu jídel jsem vůbec neznala. Také ubývají pocity hladu, naučila jsem se režimu a jsem sama sebou spokojena.</html>"
        }
        
        MyLabel {
            multiline: true
            fontFamily: "RobotoLight"
            fontSize: FontSize.Medium
            text: "<html><b>JAN Z. - 05.03.2014</b>\nNaprosto perfektní skladba jídel. Kombinace a vychucení je na úrovni pětihvězdičkového hotelu. Například dnešní kuskus s parmezánem - excelentní.</html>"
        }
    }

}