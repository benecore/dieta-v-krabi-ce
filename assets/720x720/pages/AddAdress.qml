import bb.cascades 1.2
import bb.system 1.2

import "../components"

Sheet {
    id: root
    
    signal done(bool edit, string text)
    signal canceled()
    
    onClosed: {
        destroy()
    }
    
    onOpened: {
        menoField.requestFocus()
    }
    
    property variant adress: null
    property bool edit: false
    
    BasePage {
        
        function request(response){
            var json = JSON.parse(response)
            if (json.error == 0){
                root.done(root.edit,json.data)
            }
        }
        
        property string idAdresy
        header: TitleHeader {
            headerText: root.edit ? "Editovat adresu".toUpperCase() : "Přidat adresu".toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        
        onCreationCompleted: {
            App.pridatAdresuDone.connect(request)
            App.editovatAdresuDone.connect(request)
        }
        
        ScrollView {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Container {
                topPadding: 20
                leftPadding: 20
                rightPadding: 20
                bottomPadding: 20
                horizontalAlignment: HorizontalAlignment.Fill
                
                TextField {
                    id: menoField
                    hintText: "Jméno/Firma*"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    text: adress ? adress.jmeno : ""
                    input.onSubmitted: {
                        uliceField.requestFocus()
                    }
                }
                TextField {
                    id: uliceField
                    hintText: "Ulice a č.p.*"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    text: adress ? adress.ulice : ""
                    input.onSubmitted: {
                        mestoField.requestFocus()
                    }
                }
                TextField {
                    id: mestoField
                    hintText: "Město*"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    text: adress ? adress.obec : ""
                    input.onSubmitted: {
                        pscField.requestFocus()
                    }
                }
                TextField {
                    id: pscField
                    hintText: "PSČ*"
                    inputMode: TextFieldInputMode.Text
                    input.submitKey: SubmitKey.Next
                    text: adress ? adress.psc : ""
                    input.onSubmitted: {
                        menoField.requestFocus()
                    }
                }
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    Button {
                        text: "Zrusit"
                        onClicked: {
                            root.canceled()
                        }
                    }
                    
                    Button {
                        text: root.edit ? "Editovat" : "Přidat"
                        onClicked: {
                            if (menoField.text != "" && uliceField.text != "" && mestoField.text != "" && pscField.text != ""){
                                if (!root.edit){
                                    Api.pridatAdresu(menoField.text.trim(), uliceField.text.trim(), mestoField.text.trim(), pscField.text.trim())
                                }else{
                                    Api.editovatAdresu(adress.id, menoField.text.trim(), uliceField.text.trim(), mestoField.text.trim(), pscField.text.trim())
                                }
                            }
                        }
                    }
                }
            
            }
        } // end of scrollView
    
    }

}