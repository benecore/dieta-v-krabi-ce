import bb.cascades 1.2

Container {
    id: rootContainer
    layout: DockLayout {}
    
    property int highlightFrameSize: 8
    property string highlightFrameColor: "#6ebd43"
    property alias showDivider: divider.visible
    property bool highlight: true
    property alias backColor: rootContainer.background
    property alias showNext: nextIndicator.visible
    property alias rotateNext:nextIndicator.rotationZ
    
    Divider {
        id: divider
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
    }
    
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxWidth: highlightFrameSize
        minWidth: highlightFrameSize
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Left
    }
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxWidth: highlightFrameSize
        minWidth: highlightFrameSize
        verticalAlignment: VerticalAlignment.Fill
        horizontalAlignment: HorizontalAlignment.Right
    }
    
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxHeight: highlightFrameSize
        minHeight: highlightFrameSize
        verticalAlignment: VerticalAlignment.Top
        horizontalAlignment: HorizontalAlignment.Fill
    }
    
    Container {
        layout: DockLayout {}
        visible: highlight && rootContainer.ListItem.active || highlight && rootContainer.ListItem.selected
        background: Color.create(highlightFrameColor)
        maxHeight: highlightFrameSize
        minHeight: highlightFrameSize
        verticalAlignment: VerticalAlignment.Bottom
        horizontalAlignment: HorizontalAlignment.Fill
    }
    
    Container {
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Center
        rightPadding: highlightFrameSize
        ImageView {
            id: nextIndicator
            visible: false
            imageSource: "asset:///images/expand-green_right.png"
        }
    }

}
