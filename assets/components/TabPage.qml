import bb.cascades 1.2


NavigationPane {
    id: root
    
    onPopTransitionEnded: {
        page.active = false
        page.popDone()
        console.log("DELETE PAGE")
        page.destroy()
    }
    
    
    onPushTransitionEnded: {
        page.active = true
    }
}