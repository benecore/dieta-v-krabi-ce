import bb.cascades 1.2

import "../components"


ScrollView {
    id: root
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        MyLabel {
            fontFamily: "RobotoBold"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: "Cena rozvozu".toUpperCase()
        }
        
        MyLabel {
            topMargin: 0
            multiline: true
            fontFamily: "RobotoRegular"
            text: "<html><b>Cena je 99Kč + 21% DPH/den,</b><br/>pokud si na jedno místo objednáte více jídel, cena už se zvyšuje pouze o 10Kč za jedno jídlo.</html>"
        }
        
        MyLabel {
            topMargin: 0
            multiline: true
            fontFamily: "RobotoRegular"
            text: "<html><b>Cena 99Kč + 21% DPH/den,</b><br/>platí pro celou Prahu, Prahu-východ a Prahu-západ.</html>"
        }
    }
}