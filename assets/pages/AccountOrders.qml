import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

BasePage {
    id: root
    
    
    function request(response){
        var json = JSON.parse(response)
        model.append(json.data)
    }
    
    header: TitleHeader {
        headerText: "Objednávky".toUpperCase()
        headerRightImage: true
        headerRightImageSource: "asset:///images/tabs/objednavka.png"
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        App.mojeObjednavkyDone.connect(request)
    }
    
    
    onActiveChanged: {
        if (active){
            console.log("onActiveChanged, "+root.toString())
            Api.objednavky()
        }
    }
    
    actions: [
        RefreshAction {
            onClicked: {
                model.clear()
                Api.objednavky()
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
            id: mojeDodavky
            AccountOrdersCalendar {
                onPopDone: {
                    listView.clearSelection()
                }
            }
        }
    ]
    
    
    ActivityIndicator {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        preferredHeight: 200
        preferredWidth: 200
        running: visible
        visible: Api.loading
    }
    
    
    ListView {
        id: listView
        opacity: Api.loading ? 0 : 1
        leftPadding: 20.0
        rightPadding: 20
        topPadding: 20
        bottomPadding: 20
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: model
        }
        listItemComponents: [
            ListItemComponent {
                type: ""
                ListItemMojeObjednavky {preferredHeight: 140}
            }
        ]
        onTriggered: {
            clearSelection()
            select(indexPath)
            var item = dataModel.data(indexPath)
            if (item){
                var page = mojeDodavky.createObject()
                page.objednavka = item
                activePane.push(page)
            }
        } 
    } // end of ListView
    
} // end of BasePage