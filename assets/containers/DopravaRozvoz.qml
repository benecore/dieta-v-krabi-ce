import bb.cascades 1.2

import "../components"


ScrollView {
    id: root
    
    signal zjistit
    
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    Container {
        
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        
        MyLabel {
            fontFamily: "RobotoBold"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: "Doručovací hodiny - rozvoz:".toUpperCase()
        }
        MyLabel {
            topMargin: 0
            multiline: true
            fontFamily: "RobotoRegular"
            text: "<html><b>Ne - Pá</b> v odpoledních hodinách, můžete si zvolit jednu z následujících variant časového rozpětí, na jednotlivé dny je možné tyto časy měnit.</html>"
        }
        
        MyLabel {
            fontFamily: "RobotoBold"
            fontSize: FontSize.Large
            bottomMargin: 0
            text: "Časy rozvozu pro Prahu:".toUpperCase()
        }
        Container {
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            horizontalAlignment: HorizontalAlignment.Center
            Container {
                topPadding: 5
                bottomPadding: 5
                leftPadding: 5
                rightPadding: 5
                minWidth: (Size.maxWidth - 50) / 2
                maxWidth: (Size.maxWidth - 50) / 2
                MyLabel {
                    horizontalAlignment: HorizontalAlignment.Center
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Medium
                    bottomMargin: 0
                    text: "stred".toUpperCase()
                }
                
                MyLabel {
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 0
                    multiline: true
                    fontFamily: "RobotoRegular"
                    text: "<html>15:00 - 17:00<br/>16:00 - 18:00<br/>20:00 - 22:00</html>"
                }
            } // end of Stred praha
            
            Container {
                background: App.whiteTheme ? Color.Black : Color.LightGray
                verticalAlignment: VerticalAlignment.Fill
                horizontalAlignment: HorizontalAlignment.Left
                maxHeight: Infinity                        
                minWidth: 1
                maxWidth: 1
            }
            
            Container {
                topPadding: 5
                bottomPadding: 5
                leftPadding: 5
                rightPadding: 5
                minWidth: (Size.maxWidth - 50) / 2
                maxWidth: (Size.maxWidth - 50) / 2
                MyLabel {
                    horizontalAlignment: HorizontalAlignment.Center
                    fontFamily: "RobotoLight"
                    fontSize: FontSize.Medium
                    bottomMargin: 0
                    text: "východ a západ".toUpperCase()
                }
                
                MyLabel {
                    horizontalAlignment: HorizontalAlignment.Center
                    topMargin: 0
                    multiline: true
                    fontFamily: "RobotoRegular"
                    text: "<html>19:00 – 21:00<br/>18:00 – 20:00</html>"
                }
            } // end of Vychod/Zapad praha
        } // End of root container pre Casy rozvozu
        
        ImageButton {
            horizontalAlignment: HorizontalAlignment.Center
            defaultImageSource: "asset:///images/buttons/cas-dovozu.png"
            pressedImageSource: defaultImageSource
            onClicked: {
                root.zjistit()
            }
        } // end of Button
    }
}