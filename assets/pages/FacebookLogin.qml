import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../items"


BasePage {
    id: page
    
    header: TitleHeader {
        headerText: "FACEBOOK PŘIHLÁŠENÍ"
        headerRightImage: true
        headerRightImageSource: activeTab.imageSource
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        App.facebookLoginDone.connect(activePane.pop)
        webView.storage.clear();
    }
    
    onActiveChanged: {
        if (active){
            webView.url = "https://www.facebook.com/dialog/oauth?client_id=639045186111691&redirect_uri=https://www.facebook.com/connect/login_success.html&response_type=code%20token"
        }
    }
    
    ScrollView {
        id: scrollView
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.pinchToZoomEnabled: true
        
        
        WebView {
            id: webView
            
            onNavigationRequested: {
                console.log("URL: ".concat(request.url))
                if (request.url.toString().indexOf("access_token") != -1){
                    console.log("OBSAHUJE ACCESS TOKEN")
                    App.facebookAccount(request.url)
                }
            }
            
            settings.viewport: {
                "width": "device-width",
                "initial-scale": 1.0
            }
            
            onLoadProgressChanged: {
                // Update the ProgressBar while loading.
                progressIndicator.value = loadProgress / 100.0
            }
            
            onMinContentScaleChanged: {
                // Update the scroll view properties to match the content scale
                // given by the WebView.
                scrollView.scrollViewProperties.minContentScale = minContentScale;
                
                // Let's show the entire page to start with.
                scrollView.zoomToPoint(0,0, minContentScale,ScrollAnimation.None)
            }
            
            onMaxContentScaleChanged: {
                // Update the scroll view properties to match the content scale 
                // given by the WebView.
                scrollView.scrollViewProperties.maxContentScale = maxContentScale;
            }
            
            onLoadingChanged: {
                if (loadRequest.status == WebLoadStatus.Started) {
                    // Show the ProgressBar when loading started.
                    console.log("WebView Started")
                    progressIndicator.opacity = 1.0
                } else if (loadRequest.status == WebLoadStatus.Succeeded) {
                    // Hide the ProgressBar when loading is complete.
                    console.log("WebView Succeeded")
                    progressIndicator.opacity = 0.0
                } else if (loadRequest.status == WebLoadStatus.Failed) {
                    // If loading failed, fallback a local html file which will also send a Java script message                        
                    console.log("WebView failed")
                    progressIndicator.opacity = 0.0
                }
            }
            settings.imageDownloadingEnabled: false
        } // end of webView
    
    } // end of scroll view
    
    Container {
        bottomPadding: 25
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Bottom
        
        ProgressIndicator {
            id: progressIndicator
            opacity: 0
        }
    }

} // end of BasePage