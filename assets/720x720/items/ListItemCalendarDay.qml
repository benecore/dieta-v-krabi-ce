import bb.cascades 1.2

import "../components"

ListItemBase {
    id: itemBase
    highlight: false
    backColor: itemBase.ListItem.selected || itemBase.ListItem.active ? Color.create("#ed1c45") : ListItemData.hasData ? Color.create("#6ebd43") : Color.Transparent
    
    Container {
        id: rootItem
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Top
        layout: StackLayout {}
        
        Label {
            topMargin: 0
            horizontalAlignment: HorizontalAlignment.Center
            visible: ListItemData.dayName != ""
            textStyle.color: Color.Gray
            //fontFamily: "RobotoLight"
            textStyle.fontSize: FontSize.Small
            text: ListItemData.dayName
            bottomMargin: 0
        }
        
        Label {
            topMargin: 0
            horizontalAlignment: HorizontalAlignment.Center
            //fontFamily: "RobotoLight"
            textStyle.fontSize: FontSize.Medium
            textStyle.fontWeight: ListItemData.today ? FontWeight.Bold : FontWeight.Normal
            text: ListItemData.day
            bottomMargin: 0
        }
        Label {
            topMargin: 0
            visible: text != ""
            horizontalAlignment: HorizontalAlignment.Center
            //fontFamily: "RobotoLight"
            textStyle.fontSize: FontSize.XXSmall
            text: ListItemData.canEdit ? ListItemData.hasData ? "editovat" : "přidat" : ""
            bottomMargin: 0
        }
        /*
         MyLabel {
         styleFamily: "RobotoLight"
         styleSize: FontSize.XXSmall
         visible: ListItemData.day != 6
         horizontalAlignment: HorizontalAlignment.Center
         verticalAlignment: VerticalAlignment.Bottom
         text: canEdit() ? ListItemData.data ? "editovat" : "nastavit" : ""
         }*/
    }
}