import bb.cascades 1.2


Label {
    id: root
    
    property alias fontFamily: styleDefinition.fontFamily
    property alias fontSize: styleDefinition.fontSize
    
    attachedObjects: [
        TextStyleDefinition {
            id: styleDefinition
            
            rules: [
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Black.ttf"
                    fontFamily: "RobotoBlack"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-BlackItalic.ttf"
                    fontFamily: "RobotoBlackItalic"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Bold.ttf"
                    fontFamily: "RobotoBold"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-BoldItalic.ttf"
                    fontFamily: "RobotoBoldItalic"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Italic.ttf"
                    fontFamily: "RobotoItalic"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Light.ttf"
                    fontFamily: "RobotoLight"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-LightItalic.ttf"
                    fontFamily: "RobotoLightItalic"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Medium.ttf"
                    fontFamily: "RobotoMedium"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-MediumItalic.ttf"
                    fontFamily: "RobotoMediumItalic"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Regular.ttf"
                    fontFamily: "RobotoRegular"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-Thin.ttf"
                    fontFamily: "RobotoThin"
                },
                FontFaceRule {
                    source: "asset:///fonts/Roboto-ThinItalic.ttf"
                    fontFamily: "RobotoThinItalic"
                }
            ]
            fontFamily: "RobotoRegular, sans-serif"
        }
    ]
    
    textStyle.base: styleDefinition.style
}