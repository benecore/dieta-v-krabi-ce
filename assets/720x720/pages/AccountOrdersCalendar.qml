import bb.cascades 1.2
import com.devpda.models 1.0
import Qt.labs.folderlistmodel 1.0
import QtMultimediaKit 1.1
import bb.system 1.2

import "../components"
import "../items"
import "../actions"




BasePage {
    id: root
    
    function request(response){
        root.data = response
        var json = JSON.parse(response)
        titleHeader.headerText = qsTr("Objednáno %1/%2".toUpperCase()).arg(json.vycerpano).arg(json.kredit)
        calendarModel.updateModel(json.data)
    }
    
    
    property variant objednavka
    property variant data
    property int showMonth: 0
    header: TitleHeader {
        id: titleHeader
        headerText: "Objednáno 0/0".toUpperCase()
        headerRightImage: true
        headerRightImageSource: "asset:///images/tabs/objednavka.png"
    }
    backgroundColor: Color.create("#f8f8f8")
    
    onCreationCompleted: {
        App.mojaObjednavkaDone.connect(request)
    }
    
    onActiveChanged: {
        if (active){
            Api.mojaObjednavka(objednavka.id)
        }
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: orderPage
            AccountOrderPage {
                onPopDone: {
                    detailListView.clearSelection()
                }
                onDone: {
                    activePane.pop()
                    toast.body = text
                    toast.show()
                }
                onError: {
                	activePane.pop()
                    toast.body = text
                    toast.show()
                }
                onRemoved: {
                    activePane.pop()
                    toast.body = text
                    toast.show()
                }
            }
        },
        SystemToast{
            id: toast
            autoUpdateEnabled: true
            button.label: "Zavřít"
            onFinished: {
                if (value == SystemUiResult.ButtonSelection){
                    cancel()
                }
            }
        }
    ]
    
    actions: [
        PreviousAction {
            onClicked: {
                --root.showMonth
                calendarModel.generateModel(showMonth)
                request(data)
            }
        },
        RefreshAction {
            onClicked: {
                calendarModel.generateModel()
                Api.mojaObjednavka(objednavka.id)
            }  
        },
        NextAction {
            onClicked: {
                ++root.showMonth
                calendarModel.generateModel(showMonth)
                request(data)
            }
        }
    ]
    
    
    Container {
        topPadding: 10
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: -1
            }
            
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                fontSize: FontSize.Large
                text: calendarModel.monthName + " (" + calendarModel.year + ")"
                bottomMargin: 0
            }
            
            Divider {
                topMargin: 0
                bottomMargin: 5
            }
        }
        
        ListView {
            id: listView
            layoutProperties: StackLayoutProperties {
                spaceQuota: 2
            }
            scrollRole: ScrollRole.Main
            dataModel: CalendarModel {
                id: calendarModel
                //language: Language.Czech
            }
            layout: GridListLayout {
                columnCount: 7
                cellAspectRatio: 1.0

            }
            listItemComponents: [
                ListItemComponent {
                    type: "item"
                    ListItemCalendarDay{}
                }
            ]
            onTriggered: {
                clearSelection()
                select(indexPath)
                var item = dataModel.data(indexPath)
                if (item){
                    if (item.hasData && item.canEdit){
                        popisContainer.detailData = item
                        popisContainer.edit = true
                        popisContainer.past = false
                    }else if(!item.hasData && item.canEdit){
                        popisContainer.detailData = item
                        popisContainer.edit = false
                        popisContainer.past = false
                    }else if (item.hasData){
                        popisContainer.detailData = item
                        popisContainer.edit = true
                        popisContainer.past = true
                    }else{
                        popisContainer.detailData = null
                        popisContainer.past = false
                    }
                }
            }
            bottomMargin: 0
        } // end of listView
        
        Container {
            id: popisContainer
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1.4
            }
            visible: detailData != null
            property variant detailData: null
            property bool edit: false
            property bool past: false
            onDetailDataChanged: {
                if (detailData != null){
                    detailListView.dataModel.clear()
                    detailListView.dataModel.append(detailData)
                }
            }
            Header {
                title: "Detail dodávky"
                subtitle: popisContainer.detailData.dayLongName + " (" + popisContainer.detailData.date + ")"
            }
            
            Button {
                visible: !popisContainer.edit
                horizontalAlignment: HorizontalAlignment.Center
                text: "Přidat"
                onClicked: {
                    var json = JSON.parse(root.data)
                    console.log(JSON.stringify(popisContainer.detailData))
                    var page = orderPage.createObject()
                    page.objednavkaId = objednavka.id
                    page.datum = popisContainer.detailData.date
                    page.data = popisContainer.detailData
                    page.kredit = json.kredit
                    page.vycerpane = json.vycerpano
                    page.edit = false
                    activePane.push(page)
                }
            }
            
            ListView {
                id: detailListView
                visible: popisContainer.edit
                scrollRole: ScrollRole.Main
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                dataModel: ArrayDataModel {
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ListItemDodavka {}
                    }
                ]
                onTriggered: {
                    clearSelection()
                    select(indexPath)
                    var item = dataModel.data(indexPath)
                    if (item && !popisContainer.past){
                        var json = JSON.parse(root.data)
                        console.log(JSON.stringify(item))
                        var page = orderPage.createObject()
                        page.objednavkaId = objednavka.id
                        page.datum = item.date
                        page.data = item
                        page.kredit = json.kredit
                        page.vycerpane = json.vycerpano
                        page.edit = true
                        activePane.push(page)
                    }else{
                        toast.body = "Tento den již nelze editovat"
                        toast.show()
                        clearSelection()
                    }
                }
            } // end of detailListView
        } // end of popis container  
    } // end of StackLayout container
}