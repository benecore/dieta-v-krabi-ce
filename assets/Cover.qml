import bb.cascades 1.2


Container {
    id: cover
    layout: DockLayout{}
    
    background: Color.create("#6ebd43")
    
    
    ImageView {
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        scalingMethod: ScalingMethod.AspectFit
        imageSource: "asset:///images/cover_icon.png"
    }
    
}
