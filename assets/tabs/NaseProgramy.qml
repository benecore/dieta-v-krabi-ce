import bb.cascades 1.2

import "../components"
import "../pages"

TabPage {
    id: root
    
    
    
    BasePage {
        
        
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase()
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundImage: "asset:///images/backgrounds/home-background.png"
        
        
        attachedObjects: [
            ComponentDefinition {
                id: hubnuZena
                HubnuZena {
                    onObjednat: {
                        console.log("Objednat clicked")
                        activeTab = objednavkaTab
                    }
                }
            },
            ComponentDefinition {
                id: hubnuMuz
                HubnuMuz {
                    onObjednat: {
                        console.log("Objednat clicked")
                        activeTab = objednavkaTab
                    }
                }
            },
            ComponentDefinition {
                id: zzdraveZena
                ZZdraveZena {
                    onObjednat: {
                        console.log("Objednat clicked")
                        activeTab = objednavkaTab
                    }
                }
            },
            ComponentDefinition {
                id: zzdraveMuz
                ZZdraveMuz {
                    onObjednat: {
                        console.log("Objednat clicked")
                        activeTab = objednavkaTab
                    }
                }
            }
        ]
        
        
        ScrollView {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Fill
            Container {
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 100
                    ImageButton {
                        rightMargin: 80
                        defaultImageSource: "asset:///images/menu/hubnu-zena.png"
                        pressedImageSource: "asset:///images/menu/hubnu-zena_pressed.png"
                        onClicked: {
                            var page = hubnuZena.createObject()
                            activePane.push(page)
                        }
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///images/menu/hubnu-muz.png"
                        pressedImageSource: "asset:///images/menu/hubnu-muz_pressed.png"
                        onClicked: {
                            var page = hubnuMuz.createObject()
                            activePane.push(page)
                        }
                    }
                } // End of first row icon
                
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Fill
                    topPadding: 100
                    ImageButton {
                        rightMargin: 80
                        defaultImageSource: "asset:///images/menu/zz-zena.png"
                        pressedImageSource: "asset:///images/menu/zz-zena_pressed.png"
                        onClicked: {
                            var page = zzdraveZena.createObject()
                            activePane.push(page)
                        }
                    }
                    
                    ImageButton {
                        defaultImageSource: "asset:///images/menu/zz-muz.png"
                        pressedImageSource: "asset:///images/menu/zz-muz_pressed.png"
                        onClicked: {
                            var page = zzdraveMuz.createObject()
                            activePane.push(page)
                        }
                    }
                }
            }
        }
    
    
    }
}