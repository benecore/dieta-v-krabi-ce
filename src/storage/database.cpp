/*
 * database.cpp
 *
 *  Created on: 7.3.2014
 *      Author: benecore
 */

#include "database.h"

Database *Database::_instance = 0;

Database::Database()
{
	db = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE"));
	db->setDatabaseName("data/database.db");
	bool ok = db->open();
	if(ok){
		//qDebug() << "DatabaseManager: db opened.";
	}else{
		qDebug() << "DatabaseManager: db open error.";
	}
	createTables();
}

Database::~Database()
{
	delete db;
}


Database *Database::instance()
{
	if (!_instance)
		_instance = new Database;
	return _instance;
}


void Database::destroy()
{
	if (_instance){
		delete _instance;
		_instance = 0;
	}
}


void Database::createTables()
{
	db->exec("CREATE TABLE IF NOT EXISTS login(email TEXT UNIQUE, password TEXT)");
}


bool Database::setLogin(const QString &email, const QString &password)
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("INSERT INTO login VALUES(?, ?)");
	query.addBindValue(email);
	query.addBindValue(password);

	bool exec = query.exec();

	if (exec){
		qDebug() << "Login saved";
		emit databaseChanged();
	}else{
		qWarning() << "Unable to save login details" << db->lastError();
	}
	return exec;
}


QVariantMap Database::getLogin()
{
	QVariantMap map;
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("SELECT * FROM login");

	bool exec = query.exec();

	while (query.next()){
		qDebug() << query.value(0);
		map.insert("email", query.value(0));
		map.insert("password", query.value(1));
	}
	if (exec){
		qDebug() << "Getlogin success";
	}else{
		qWarning() << "Unable to get login details" << db->lastError();
	}
	return map;
}


bool Database::clearLogin()
{
	if (!db->isOpen())
		db->open();
	QSqlQuery query;
	query.prepare("DELETE FROM login");

	bool exec = query.exec();

	if (exec){
		qDebug() << "LOGIN DELETED";
		emit databaseChanged();
	}else{
		qWarning() << "Unable to delete login details" << db->lastError();
	}
	return exec;
}
