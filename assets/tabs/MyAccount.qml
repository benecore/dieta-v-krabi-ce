import bb.cascades 1.2
import bb.system 1.2

import "../components"
import "../actions"
import "../items"
import "../pages"
import "../sheets"

TabPage {
    id: root
    
    onPopTransitionEnded: {
        listView.clearSelection()
    }
    
    BasePage {
        id: page
        
        
        function request(response){
            var json = JSON.parse(response)
            if (json.error == "OK"){
                toast.body = "Přihlášení úspěšné"
                if (pamatatPrihlasenie.checked){
                    console.log("HESLO MD5: ".concat(App.md5(hesloField.text.trim())))
                    DB.setLogin(emailField.text.trim(), App.md5(hesloField.text.trim()))
                }
                logged = true
                toast.show()
                addAction(signOutAction.createObject())
            }else{
                toast.body = "Nepodařilo se přihlásit"
                logged = false
            }
            toast.show()
        }
        
        
        function prihlasit(){
            if (emailField.text == ""){
                toast.body = "Zadejte email"
                toast.show()
                return;
            }else if (hesloField.text == ""){
                toast.body = "Zadejte heslo"
                toast.show()
                return;
            }
            Api.prihlasenie(emailField.text.trim(), hesloField.text.trim())
        }
        
        function loggedChanged(){
            console.log("Logged changed: ".concat(DB.isLogged))
            page.logged = DB.isLogged
        }
        
        property bool logged: DB.isLogged
        header: TitleHeader {
            headerText: activeTab.title.toUpperCase();
            headerRightImage: true
            headerRightImageSource: activeTab.imageSource
        }
        backgroundColor: Color.create("#f8f8f8")
        onCreationCompleted: {
            if (logged){
                addAction(signOutAction.createObject())
            }
            DB.databaseChanged.connect(loggedChanged)
            App.prihlasenieDone.connect(request)
        }
        
        onActiveChanged: {
            if (active){
            }
        }
        
        attachedObjects: [
            SystemToast {
                id: toast
                button.label: "Zavřít"
                autoUpdateEnabled: true
                onFinished: {
                    if (value == SystemUiResult.ButtonSelection){
                        toast.cancel()
                    }
                }
            },
            ComponentDefinition {
                id: signOutAction
                SignOutAction {
                    onClicked: {
                        DB.clearLogin()
                        pamatatPrihlasenie.checked = false
                        page.removeAllActions()
                        page.logged = false
                    }
                }
            },
            ComponentDefinition {
                id: mojeObjednavky
                AccountOrders {
                    
                }
            },
            ComponentDefinition {
                id: nastavenieUctu
                AccountSettings {
                    
                }
            },
            ComponentDefinition {
                id: registerSheet
                Register {
                    onDone: {
                        page.logged = true
                        addAction(signOutAction.createObject())
                    }
                }
            },
            ComponentDefinition {
                id: facebookLogin
                FacebookLogin {
                    
                }
            },
            SystemPrompt {
                id: emailDialog
                title: "Facebook email"
                rememberMeChecked: false
                includeRememberMe: false
                inputField.emptyText: "email*"
                onFinished: {
                    if (value == SystemUiResult.ConfirmButtonSelection){
                        var text = inputFieldTextEntry().toLowerCase().trim()
                        console.debug("Input text".concat(text))
                        App.facebookMail = inputFieldTextEntry().trim();
                        var page = facebookLogin.createObject()
                        activePane.push(page)
                    }
                }
                confirmButton.label: qsTr("Tag it")
                returnKeyAction: SystemUiReturnKeyAction.Done
                body: "Zadejte váš Facebook email pro kontrolu"
                inputField.inputMode: SystemUiInputMode.Email
            }
        ]
        
        ScrollView {
            scrollRole: ScrollRole.Main
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            Container {
                leftPadding: 20.0
                rightPadding: 20.0
                topPadding: 20.0
                bottomPadding: 20.0
                horizontalAlignment: HorizontalAlignment.Fill
                verticalAlignment: VerticalAlignment.Fill
                Container {
                    id: loginContainer
                    visible: !page.logged
                    TextField {
                        id: emailField
                        hintText: "email"
                        inputMode: TextFieldInputMode.EmailAddress
                        input.submitKey: SubmitKey.Next
                        input.onSubmitted: {
                            hesloField.requestFocus()
                        }
                    }
                    
                    TextField {
                        id: hesloField
                        hintText: "heslo"
                        inputMode: TextFieldInputMode.Password
                        input.submitKey: SubmitKey.EnterKey
                        input.onSubmitted: {
                            if (emailField.text == "" || hesloField)
                                page.prihlasit()
                        }
                    }
                    
                    CheckBoxRow {
                        id: pamatatPrihlasenie
                        text: "Zapamatovat přihlášení"
                    }
                    
                    Container {
                        topMargin: 20
                        layout: StackLayout {
                            orientation: LayoutOrientation.LeftToRight
                        }
                        horizontalAlignment: HorizontalAlignment.Center
                        Button {
                            text: "Přihlášení"
                            onClicked: {
                                page.prihlasit()
                            }
                        }
                        
                        Button {
                            text: "Registrace"
                            onClicked: {
                                var sheet = registerSheet.createObject()
                                sheet.open()
                            }
                        }
                    } // end of buttons
                    
                    ImageButton {
                        visible: false
                        horizontalAlignment: HorizontalAlignment.Center
                        defaultImageSource: "asset:///images/buttons/facebook-login.png"
                        pressedImageSource: defaultImageSource
                        onClicked: {
                            emailDialog.show()
                        }
                    }
                    
                } // end of login container
                ListView {
                    id: listView
                    visible: page.logged
                    preferredHeight: 280
                    onCreationCompleted: {
                        var values = [{"title": "Kalendář jídel", "subtitle": "Nastavení objednávek"},
                            {"title": "Nastavení účtu", "subtitle": "Nastavení účtu a doručovacích adres"}]
                        model.append(values)
                    }
                    horizontalAlignment: HorizontalAlignment.Fill
                    verticalAlignment: VerticalAlignment.Fill
                    dataModel: ArrayDataModel {
                        id: model
                    }
                    listItemComponents: [
                        ListItemComponent {
                            type: ""
                            ListItemAccount{preferredHeight: 140}
                        }
                    ]
                    onTriggered: {
                        clearSelection()
                        select(indexPath)
                        var item = dataModel.data(indexPath)
                        if (item){
                            var page
                            switch (parseInt(indexPath)){
                                case 0:
                                    console.log("Here i am")
                                    page = mojeObjednavky.createObject()
                                    break;
                                case 1:
                                    page = nastavenieUctu.createObject()
                                    break;
                            }
                            activePane.push(page)
                        }
                    }
                } // end of listview
            
            } // end of StackLayout container
        } // end of ScrollView
    } // end of BasePage

} // end of TabPage